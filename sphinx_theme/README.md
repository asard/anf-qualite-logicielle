# Thème Sphinx ANF Qualité Logicielle

## Installation

```bash
pip install --index-url https://test.pypi.org/simple/ anf-qualite-theme
```

## Utilisation

Dans le fichier `conf.py` de votre projet, ajouter la ligne suivante :

```python
html_theme = 'anf-qualite-theme'
```
