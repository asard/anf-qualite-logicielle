import pathlib

from sphinx.application import Sphinx

THEME_PATH = pathlib.Path(__file__).parent.resolve()

def setup(app: Sphinx):
    print('setup anf_qualite_theme')
    app.add_html_theme('anf_qualite_theme', THEME_PATH)
