# TP1 - Mise en place de l'environnement et introduction à la qualité logicielle

Cette formation s'appuie sur une mise en situation qui vous permettra de mieux comprendre les enjeux de la qualité logicielle et de mettre en pratique les notions abordées durant les présentations. Le but de ce premier TP est de mettre en place l'environnement de travail qui sera utilisé tout au long de la formation.

----

## Contexte

Vous faites partie d'une équipe de recherche qui travaille sur un projet de recherche (fictif) intitulé "Impact des Pratiques Humaines sur le Karma". L'objectif principal de ce projet de recherche est d'étudier, en se basant sur des principes de microbiologie quantique en milieu relativistique, s'il existe une corrélation entre la consommation abusive de raclette et la probabilité de réincarnation en limace.

Ce projet a été mené par un brillant post-doc, qui a développé un code pour analyser les données expérimentales. Malheureusement, ce post-doc a quitté le laboratoire pour rejoindre une start-up, et il est parti en laissant derrière lui un code non documenté. La mission de votre équipe est de reprendre ce code, de le comprendre, de le documenter et de l'améliorer pour qu'il puisse être utilisé par la communauté scientifique. Il s'agira aussi de mettre en place des bonnes pratiques de développement pour éviter que ce genre de situation ne se reproduise à l'avenir.

----

## Étape 0: Mise en place des clés SSH

Afin de récupérer le code du projet, vous allez devoir utiliser Git. Pour cela, vous devez d'abord mettre en place une paire de clés SSH et l'importer dans votre compte Gitlab. Pour cela, suivez les étapes suivantes:

:arrow_forward: Sur votre ordinateur :computer:

- Ouvrez un terminal.
- Placez-vous dans votre `$HOME`, si vous n'y êtes pas déjà :

  ```bash
  cd
  ```

- Créez un répertoire `.ssh`, s'il n'existe pas déjà :

  ```bash
  mkdir .ssh
  cd .ssh
  ```

- Créez une nouvelle paire de clés SSH :

  ```bash
  ssh-keygen -t ed25519 -C "commentaire" -f ma_cle_ssh
  ```

- Créez un fichier `config` (ou complétez l'existant) dans le répertoire `.ssh`, contenant les éléments suivants :

  ```bash
  Host gitlab.in2p3.fr
    User git
    PasswordAuthentication no
    IdentityFile ~/.ssh/ma_cle_ssh
    IdentitiesOnly yes
    PubkeyAuthentication yes
    ForwardX11 no
    ForwardAgent no
  ```

:arrow_forward: Sur Gitlab :fox:

- Importez la clé publique précédemment générée dans [votre compte Gitlab](https://gitlab.in2p3.fr/-/profile/keys).

----

## Étape 1: Constitution des groupes de travail

:arrow_forward: Travail en groupe :busts_in_silhouette:

Pour simuler une équipe de recherche, nous vous invitons à former des groupes de 5 personnes, en essayant de mélanger les personnes de différents laboratoires. Une fois les groupes formés, vous vous assignerons ensuite un numéro de groupe qui vous permettra d'accéder au dépôt Gitlab de votre équipe. Un membre du groupe sera "Responsable du projet", il sera propriétaire d'un groupe situé ici :
https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle-tp

- le responsable de groupe va initier un projet **privé** dans le groupe :
> Create a new project > Import Project > Repository by URL

Url du projet à dupliquer : https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle-tp/karma_analysis.git

- Vérifier dans les `Settings` que le projet est bien **privé**
- Rajouter vos collaborateurs dans le projet (onglet `Members` dans le menu)
- Vérifiez que vos collaborateurs ont bien tous accès au projet et que personne d'autre n'y a accès 


----

## Étape 2: Récupérer les travaux du brillant post-doc

Pour récupérer les travaux du brillant post-doc, vous allez devoir utiliser Git. Pour cela, suivez les étapes suivantes:

:arrow_forward: Sur votre ordinateur :computer:

- Ouvrez un terminal.

- Placez-vous dans le répertoire de votre choix qui contiendra le dossier du projet de recherche (un répertoire `projet` par exemple) :

  ```bash
  cd chemin/vers/projet
  ```

- Clonez le dépôt Git du projet de recherche en prenant soin de remplacer `X` par le numéro de votre groupe  :

  ```bash
  git clone git@gitlab.in2p3.fr:ri3/ecole-info/2023/anf-qualite-logicielle-tp/groupe_X/karma_analysis.git
  ```

- Placez-vous dans le répertoire du projet de recherche :

  ```bash
  cd karma_analysis
  ```

- Listez les branches disponibles localement et vérifiez que vous êtes bien sur la branche `main` :

  ```bash
  git branch
  ```

Bien joué ! Vous avez récupéré les travaux du brillant post-doc !

----

## Étape 3: Comprendre le code

Maintenant que vous avez récupéré les travaux du post-doc, vous allez devoir comprendre ce que fait ce code.

:arrow_forward: Travail en groupe :busts_in_silhouette:

Rappelez-vous, vous faites partie d'une équipe de recherche, regroupez-vous avec vos collègues pour analyser ensemble le code.

:arrow_forward: Sur votre ordinateur :computer: ou sur le dépôt Gitlab :fox:

Commencez par ouvrir le fichier `karma_analysis.py`. Tentez d'identifier les différentes étapes du code, en vous aidant des commentaires et des noms de variables.

<details>
  <summary>Solution</summary>

Le code est découpé en fonctions et permet de lire des données simulées sauvegardées au format JSON, de les analyser à l'aide de plusieurs modèles (linéaire, quadratique, sinusoidal) et d'afficher les données et le meilleur modèle.

La fonction `main` permet d'utiliser le code en ligne de commande, en spécifiant le chemin vers le fichier JSON à analyser.

```mermaid
graph TD

subgraph Lecture des Entrées
A[Analyse des Arguments de Ligne de Commande]
B[Lecture des Données à partir du Fichier JSON]
end

subgraph Comparaison des Modèles
C[Conversion du Karma en Probabilité de Réincarnation]
D[Ajustement du Modèle en Utilisant l'Ajustement de Courbe]
D1[Modèle Linéaire]
D2[Modèle Quadratique]
D3[Modèle Sinusoïdal]
E[Calcul de l'Erreur Quadratique Moyenne]
F[Détermination du Meilleur Modèle]
end

subgraph Traçage
G[Traçage des Données et des Prédictions du Modèle]
end

A --> B
B --> C
C --> D
D --> D1
D --> D2
D --> D3
D1 --> E
D2 --> E
D3 --> E
E --> F
F --> G
```

</details>

----
## Étape 4: Identifier les problèmes

:arrow_forward: Toujours en groupe :busts_in_silhouette:

Maintenant que vous avez compris ce que fait le code, vous allez devoir identifier les problèmes de qualité logicielle et les éventuels bugs qui sont présents dans ce code. Nous vous invitons à noter ces problèmes en rajoutant une section "Qualité Logicielle : Problèmes identifiés" dans le ReadMe au projet que vous pourrez éditer directement dans Gitlab et les mettre en commun avec votre équipe pour être mesure de les corriger ultérieurement.

<details>
  <summary>Solution</summary>

Parmi les problèmes identifiables, on peut citer :

- Documentation lacunaire ( docstrings manquants).
- Manque de commentaires.
- Noms de variables non explicites.
- Utilisation d'un chemin de données en dur au lieu de l'argument donné en ligne de commande.
- Bug dans la fonction de calcul de la probabilité de réincarnation en limace.
- Pas ou peu de tests, typage.
- Pas de règles de codage (mix de camelCase et snake_case, espaces avant et après les opérateurs, etc.).
- Plusieurs imports sur la même ligne et import de modules inutilisés.
- Pas de structuration sous forme de package Python pyproject.toml/setup.py.
- Pas de liste des dépendances (requirements.txt/pyproject.toml).

</details>

----
## Étape 5: Définir une documentation minimale

:arrow_forward: Sur votre document partagé :notepad_spiral:

En préparation de la session de demain, vous allez réfléchir à une documentation minimale pour ce projet. Cette documentation sera, dans un premier temps, stockée sur votre ReadMe. Nous verrons demain comment la convertir en une documentation plus complète, comment l'intégrer dans le projet, et comment travailler en équipe sur cette documentation via Git et Gitlab.

<details>
  <summary>Solution</summary>

Une documentation minimale prend souvent la forme d'un README, qui contient des informations générales sur le projet :

- Titre du projet.
- Description du projet/contexte.

ainsi que des informations techniques sur le code et son utilisation :

- Procédure d'installation (installation des dépendances, installation du package).
- Procédure d'utilisation (utilisation de la CLI, utilisation des fonctions).
- Problèmes connus (bugs, limitations, etc.).
- Comment contribuer au projet (règles de codage, tests, etc.).

Cette liste n'est pas exhaustive, et vous pouvez ajouter d'autres informations si vous le souhaitez.

</details>
