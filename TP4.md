# TP4 - Intégration et Déploiement Continus (CI/CD)

**Note: ce TP utilise les mots clés de l'interface de Gitlab dans sa version anglaise.  Vous pouvez basculer l'interface de Gitlab en version anglaise, via [les préférences de votre compte utilisateur](https://gitlab.in2p3.fr/-/profile/preferences).**

----

## Préambule

**Note: Le TP est réalisable entièrement dans Gitlab avec l'aide de l'éditeur de pipeline et les fichiers utilisés peuvent aussi se rajouter avec l'interface de Gitlab.**

:arrow_forward: Sur Gitlab :cat:

- Pour éviter de vous emmêler les pinceaux avec vos collègues, nous vous conseillons de faire un [`fork`](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) du projet `karma_analysis` dans votre espace personnel.
- Vérifiez dans les préférences générales du projet (dans votre espace personnel) si la fonctionalité _"CI/CD"_ est bien activée et, si nécessaire, activez la via le menu `Settings > General > Visibility, project features, permissions`.
- A partir de maintenant, vous allez simplement éditer et commiter le fichier `.gitlab-ci.yml`.
- Des liens d'aide sont disponibles sur chaque mot clé et pointent vers la documentation Gitlab officielle.

----

## Etape 0: Installation de votre propre runner

**Note: Il est intéressant d'installer son propre runner pour éviter les temps d'attente dans la file des runners publics ou partagés, et éventuellement gagner en performance (ressources dédiées). Dans le cadre ce TP, ce n'est pas obligatoire mais fortement recommandé.**

:arrow_forward: Sur Gitlab :cat:

- Enregistrement: <https://docs.gitlab.com/runner/register/index.html#docker>
  - Pour les valeurs (`url` et `token`), voir la section `Specific Runners` dans le menu `Settings > CI/CD > Runners` de votre projet Gitlab.
- Installation: <https://docs.gitlab.com/runner/install/docker.html>
  - lancer le container docker gitlab-runner ([option 1 sur la doc docker](https://docs.gitlab.com/runner/install/docker.html#option-1-use-local-system-volume-mounts-to-start-the-runner-container))
  - executer le container : `docker exec -it CONTAINER_ID bash`
  - lancer la commande `gitlab-runner register  --url https://gitlab.in2p3.fr  --token TOKEN` donné par gitlab lors de l'enregistrement :point_up:
- Revenir sur vos gitlab settings, le runner devrait maintenant être visible
  - Pensez ensuite à désactiver les _"shared runners"_ pour votre projet, pour n'utiliser que le runner que vous venez d'enregistrer (ou bien utilisez des tags).

----

## Etape 1: Stages et Pipelines

:arrow_forward: Sur Gitlab :cat:

- Editez le fichier `.gitlab-ci.yml` à la racine de votre projet ou utilisez le bouton `CI/CD configuration`.
- Le fichier `.gitlab-ci.yml` contient deja un [job]("https://docs.gitlab.com/ee/ci/yaml/#jobs") `pages` ajouté lors du TP 2 pour publier automatiquement la documentation générée par Sphinx. Commentez l'ensemble du contenu du fichier `.gitlab-ci.yml` pour le moment, nous allons le remplir progressivement.
- Ajoutez un seul [stage]("https://docs.gitlab.com/ee/ci/yaml/#stages) nommé `test` ainsi qu'un [job]("https://docs.gitlab.com/ee/ci/yaml/#jobs") nommé `job_test` associé ce stage.
- Le job devra afficher _"Hello world"_ et afficher la date.
- Après le commit, visualisez le pipeline et l'execution du job sur votre runner dans Gitlab.

<details>
<summary>Solution</summary>
<p>

- Contenu du fichier `.gitlab-ci.yml`:

  ```yaml
  stages:
  - test

  job_test:
    stage: test
    script:
    - echo "Hello world"
    - date
  ```

</p>
</details>

- Le runner que vous avez installé sur votre machine (ou ceux mis à disposition sur l'instance Gitlab de l'IN2P3) utilisent _"l'executor"_ `docker`. Ainsi, les jobs sont exécutés dans des conteneurs Docker.
- Vous pouvez choisir le conteneur à utiliser avec le mot-clé `image`.
- Pour l'execution précédente, l'image par défaut a été utilisée, recommencez en utilisant spécifiquement l'image `ubuntu`.

<details>
<summary>Solution</summary>
<p>

- Contenu du fichier `.gitlab-ci.yml`:

  ```yaml
  image: ubuntu
  stages:
  - test

  job_test:
    stage: test
    script:
    - echo "Hello world"
    - date
  ```

</p>
</details>

- Un pipeline est défini à l'aide de plusieurs [stages]("https://docs.gitlab.com/ee/ci/yaml/#stages").
- Par défaut, plusieurs jobs de la même étape (ie. du même _"stage"_) pourront être exécutés en parallèle, si les ressources et l'orchestrateur le permettent.
- En revanche, l'exécution d'une étape dépend du bon déroulement de l'étape précédente, à moins d'en autoriser explicitement l'échec avec le paramètre [_"allow_failure"_]("https://docs.gitlab.com/ee/ci/yaml/#allow_failure").
- Vous allez créer un pipeline avec 3 stages, utilisant une image `ubuntu` comme environnement d'exécution **[Solution 1]**:
  - Le premier va afficher la date.
  - Le deuxième va afficher le contenu du répertoire courant (eg. `ls -l`).
  - Le troisième va donner des informations système sur l'environnement d'exécution (eg. `uname -r`).
- Introduisez une erreur dans le script du stage 2: faites appel à une commande non existante (eg. `ms`).
  - Que se passe t'il lors du troisième stage ?
- Faites en sorte que le troisième stage soit executé, malgré l'échec du deuxième **[Solution 2]**.

<details>
<summary>Solution 1</summary>
<p>

- Contenu du fichier `.gitlab-ci.yml`:

  ```yaml
  image: ubuntu

  stages:
  - test1
  - test2
  - test3

  job_test1:
    stage: test1
    script:
    - date

  job_test2:
    stage: test2
    script:
    - ls

  job_test3:
    stage: test3
    script:
    - uname -r
  ```

</p>
</details>

<details>
<summary>Solution 2</summary>
<p>

- Contenu du fichier `.gitlab-ci.yml`:

  ```yaml
  image: ubuntu
  stages:
  - test1
  - test2
  - test3

  job_test1:
    stage: test1
    script:
    - date

  job_test2:
    stage: test2
    script:
    - ms
    allow_failure: true

  job_test3:
    stage: test3
    script:
    - uname -r
  ```

</p>
</details>

- Le mot clé [_"rules"_]("https://docs.gitlab.com/ee/ci/yaml/#rules") permet de définir des conditions d'exécution pour les jobs définis.
- Reprenez l'exemple précédent des 3 stages, et faites en sorte que:
  - Le premier stage soit toujours executé.
  - Le deuxième stage soit executé uniquement pour la branche `develop`.
  - Le troisième stage soit executé sur toutes les branches, sauf `develop`, et seulement pour les pipelines déclenchés lors d'une action de _"tag"_.
- Vérifiez les exécutions sur deux branches (eg. `develop` et `master`).

**Note: Certaines [variables prédéfinies dans les pipelines de Gitlab CI](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) pourraient vous être utiles...**

<details>
<summary>Solution</summary>
<p>

- Contenu du fichier `.gitlab-ci.yml`:

  ```yaml
  image: ubuntu
  stages:
  - test1
  - test2
  - test3

  job_test1:
    stage: test1
    script:
    - date

  job_test2:
    stage: test2
    script:
    - ls
    rules:
    - if: $CI_COMMIT_REF_NAME == "develop"

  job_test3:
    stage: test3
    script:
    - uname -r
    rules:
    - if: $CI_COMMIT_REF_NAME != "develop" && $CI_COMMIT_TAG != null
  ```

</p>
</details>

----

## Etape 2: automatisation des tests et génération de la documentation

**Notes: Vous allez maintenant automatiser la partie de génération automatique des tests et de la documentation vue lors des TPs précédents. Les commandes à exécuter sont les mêmes mais dans l'environnement conteneurisé des Gitlab Runners.**

:arrow_forward: Sur Gitlab :cat:

### Problème 1: `pylint` et `pytest`

- Vous allez désormais utiliser l'image `python:3` pour exécuter le pipeline et il va falloir installer les modules nécessaires !
- Définissez un job qui execute `pylint` sur le projet `karma_analysis` 
- Que se passe t'il lors de ce stage ? Peut-être qu'il faut le considérer comme non-bloquant...
- Ajoutez maintenant un job qui exécute `pytest`.

<details>
<summary>Solution</summary>
<p>

- Contenu du fichier `.gitlab-ci.yml`:

  ```yaml
  image: python:3
  stages:
  - lint
  - test

  job_lint:
    stage: lint
    before_script:
    - pip install pylint
    script:
    - pylint src/karma_analysis.py
    allow_failure: true

  job_test:
    stage: test
    before_script:
    - pip install pytest pytest-cov
    script:
    - pytest --doctest-modules src/karma_analysis.py tests/test_karma_analysis.py
  ```

</p>
</details>


### Problème 2: Vérification du typage avec `mypy`

- Ajoutez un job qui exécute `mypy` sur le projet `karma_analysis`.

<details>
<summary>Solution</summary>
<p>


- Contenu du fichier `.gitlab-ci.yml`:

  ```yaml
  job_mypy:
    stage: lint
    before_script:
    - pip install mypy
    script:
    - mypy src/karma_analysis.py
    allow_failure: true
  ```
</p>
</details>

### Problème 3: création d'un joli badge de couverture de code pour votre projet

- Vous allez maintenant ajouter un job coverage en utilisant `pytest` et `coverage`.
- Exécutez les tests avec l'option `--cov-report term` pour générer un rapport de couverture dans le terminal.
- Rajouter dans votre job l'expression régulière avec le mot de passe `coverage` : `'/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'

- Vous pouvez désormais rajouter un badge de couverture dans le menu `Settings > General > Badges`.

<details>
<summary>Solution code coverage</summary>
<p>

- Contenu du fichier `.gitlab-ci.yml`:

  ```yaml
  # ...

  test_with_coverage:
    stage: test
    before_script:
      - pip install pytest pytest-cov
    script:
      - pytest --cov-report term --cov=karma_analysis --doctest-modules src/karma_analysis.py tests/test_karma_analysis.py

  ```
</p>
</details>

<details>
<summary>Solution badges</summary>
<p>

- Paramètres à utiliser:

  - Name: `Coverage`
  - Link: `https://gitlab.in2p3.fr/%{project_path}/-/commits/%{default_branch}`
  - Url: `https://gitlab.in2p3.fr/%{project_path}/badges/%{default_branch}/coverage.svg`

</p>
</details>

----

### Problème 4: ajout des rapports dans SonarQube

- Redémarrez votre instance SonarQube sur votre machine:

  ```bash
  docker restart sonarqube
  ```

- Rajoutez à l'aide de l'interface de SonarQube un projet tp-3 et générez un _"token"_.
- Dans Gitlab créez les variables suivantes dans  `Settings > CI/CD > Variables`:
  - `SONAR_URL`: `http://<Ip_de_Votre_Machine>:9000` où `<Ip_de_Votre_Machine>` est votre adresse IPv4.
  - `SONAR_TOKEN`: Token généré à l'étape précédente, pensez à activer l'option de _"masquage"_ de la variable afin d'éviter qu'elle n'aparaisse dans les logs des jobs Gitlab.
- Rajoutez un stage et un job:
  - En utilisant l'image `sonarsource/sonar-scanner-cli:latest`.
  - Afin de scanner le code dans le répertoire `src` avec en paramètre les variables `SONAR_URL` et `SONAR_TOKEN`.
  - Vérifiez l'ajout de l'analyse sur le serveur.

<details>
<summary>Solution</summary>
<p>

- Contenu du fichier `.gitlab-ci.yml`:

  ```yaml
    job_sonar:
      stage: sonar
      image:
        name: sonarsource/sonar-scanner-cli:latest
      script:
        - cd src
        - sonar-scanner  -Dsonar.projectKey=tp-3  -Dsonar.host.url=${SONAR_URL}  -Dsonar.login=${SONAR_TOKEN}
  ```

</p>
</details>

----

### Problème 5: création de la documentation et utilisation des Pages de Gitlab

Nous allons maintenant reprendre l'automatisation de la documentation vue lors du TP 2. Vous pouvez vous aider du code précédement commenté ou repartir de zéro pour mieux appréhender chaque étape.

- Ajoutez un stage de build de la documentation:
- Installez sphinx, myst-parser et le theme `rtd` (sphinx-rtd-theme) dans le `before_script`.
- Générez maintenant la documentation avec `sphinx-build` dans le répertoire `public`.
- Créez un [artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) comprenant le contenu du dossier `public`.
- Visualisez le contenu des artefacts dans l'interface Gitlab (ie. détails du job).
- Renommez votre job en "pages" et vérifiez après un déploiement réussi, l'accès à vos "pages" via le menu `Settings > Pages`.

<details>
<summary>Solution</summary>
<p>

- Contenu du fichier `.gitlab-ci.yml`:

  ```yaml
  image: python:3
  stages:
  # ... other stages
  - deploy


  # ... other jobs

  pages:
    stage: deploy
    before_script:
    - pip install sphinx sphinx-rtd-theme myst-parser
    script:
    - sphinx-build -b html docs public
    artifacts:
      paths:
      - public
  ```

</p>
</details>

----

### Bonus : Publiez votre package sur le registry Gitlab

- Ajoutez un stage de publication de votre package sur le registry Gitlab.
- Vérifiez que votre package est bien publié dans le registry Gitlab.
- Essayez d'installer votre package depuis le registry Gitlab.


### Bonus: visualisez le rapport de couverture dans la doc

- Modifiez le stage précédent pour générer la couverture de code au format html.
- Utilisez l'option artifacts pour stocker les fichiers résultants.
- Rajoutez dans le job pages les fichiers du rapport générés précédemment dans un sous-répertoire du dossier `public` appelé `_static`.
- Modifiez le fichier `rst` pour pointer vers le rapport de couverture de code.

### Bonus: transformer votre pipeline pour utiliser un DAG

- Donnez un peu de souplesse à l'execution de votre pipeline en utilisant un DAG plutôt que les stages séquentiels.  
  - [Documentation des DAGs (Directed Acyclic Graphs)](https://docs.gitlab.com/ee/ci/directed_acyclic_graph/).
