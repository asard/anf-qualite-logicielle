# TP2 - Planifier et documenter un projet

Le TP 2 simule le travail d'un groupe de recherche qui aurait repris le projet du post-doc et qui souhaite repartir sur de bonnes bases en ajoutant la documentation manquante et en corrigeant les problèmes identifiés dans le code. Le TP 2 sera aussi l'occasion de mettre en avant l'utilisation des `issues` dans GitLab en créant des tickets pour chaque tâche à réaliser (ex : correction de bug, amélioration, etc.).

**Note: ce TP utilise les mots-clés de l'interface de GitLab dans sa version anglaise. Vous pouvez basculer l'interface de GitLab en version anglaise via [les préférences de votre compte utilisateur](https://gitlab.in2p3.fr/-/profile/preferences).**

---

## Étape 1 : création de votre propre branche

:arrow_forward: Sur GitLab :cat:

- Dans un premier temps, nous allons protéger la branche `main` de votre dépôt.
- L'un des membres de l'équipe doit se rendre dans la section `Settings > Repository > Protected branches`, vérifiez que votre branche `main` est bien protégée, personne ne doit pouvoir "pousser" ni "merger", à moins d'avoir le rôle de `maintainer`.

**Note: La fonctionnalité de protection de branches est fournie par GitLab et non par Git.**

:arrow_forward: Localement avec Git :computer:

- Ouvrez un terminal.

- Placez-vous dans le répertoire du projet de recherche :

  ```bash
  cd path/to/projet/karma_analysis
  ```

- Choisissez un nom de branche, par exemple `doc/contrib_my_username`.

- Listez les branches disponibles localement :

  ```bash
  git branch
  ```

- Créez votre branche `doc/contrib_my_username` et vérifiez la liste :

  ```bash
  git switch -c doc/contrib_my_username
  git branch
  ```

- Votre branche n'existe que localement sur votre machine pour le moment. Poussez-la dans votre repo distant sur GitLab :

  ```bash
  git push -u origin doc/contrib_my_username
  ```

**Note: Cette dernière commande vous permet désormais de faire de simples `git push` sur cette branche sans ré-spécifier l'origine et le nom de la branche.**

:arrow_forward: Sur GitLab :cat:

- Vérifiez l'existence de la branche `doc/contrib_my_username` sur GitLab !
- Maintenant, l'un des membres de l'équipe va créer une branche `develop` non protégée (directement via l'interface de GitLab).

:arrow_forward: Localement avec Git (à réaliser par tous les membres de l'équipe) :computer:

- Synchronisez le repo local avec le repo distant sur GitLab.
- Vous allez voir dans les logs apparaître la branche distante :

  ```bash
  git fetch
  ```

- Listez les branches en cours, la branche `develop` n'est pas visible !

  ```bash
  git branch
  ```

- Pour lister les branches locales et distantes, testez :

  ```bash
  git branch --all
  ```

- Associez la branche locale et distante et listez les branches :

  ```bash
  git switch develop
  git branch
  ```

**Note: Attention au nom de vos branches, ils doivent idéalement être significatifs (avoir un sens pour vous et vos collaborateurs) et surtout uniques (c'est-à-dire, ne pas déjà exister sur le repo distant), sinon gare aux conflits !**

---

## Étape 2 : Génération d'un squelette de documentation avec Sphinx

:arrow_forward: Localement avec Git :computer:

- Utilisons la branche de travail `doc/contrib_my_username` créée précédemment.
- Examinez le statut local des fichiers :

  ```bash
  git status
  ```

- Si Sphinx n'est pas installé, installez-le (il est recommandé [d'utiliser un `virtualenv` python](https://docs.python.org/3/library/venv.html)) :

  ```bash
  pip install sphinx
  ```

- Utilisez Sphinx pour générer un squelette de documentation dans le dossier `docs` :

  ```bash
  sphinx-quickstart docs
  ```

- Répondez aux questions posées par Sphinx, en gardant les valeurs par défaut, sauf pour les options suivantes :

  ```bash
  > Separate source and build directories (y/n) [n]: y
  > Project name: Karma Analysis
  > Author name(s): Les noms des contributeurs de la documentation
  > Project release []: 1.0
  ```

- Revérifiez l'état du repo, vous allez voir que le dossier `docs` a été créé et que son contenu n'est pas versionné par git.

  ```bash
  git status
  ```

- Référencez le dossier dans l'index local et réaffichez l'état de ce dernier :

  ```bash
  git add docs
  git status
  ```

- Sauvegardez ce changement dans votre branche `doc/contrib_my_username` et revérifiez son état :

  ```bash
  git commit -m 'Message descriptif sur les modifications'
  git status
  ```

- Poussez les changements de cette branche vers le repo distant sur GitLab, puis revérifiez le statut du repo :

  ```bash
  git push
  git status
  ```

:arrow_forward: Sur GitLab :cat:

- Vérifiez l'existence du fichier dans votre branche sur GitLab.
- Sélectionnez l'onglet "Merge Request" (menu latéral), puis le bouton "New Merge Request", puis spécifiez la branche source `doc/contrib_my_username` ainsi que la branche cible `develop`.
- Remplissez le formulaire puis validez la création de la "merge request".
- Examinez les options disponibles :
  - Suppression de la branche source.
  - Fusion des commits au merge (squash).
  - Approbation externe.
  - Revue de code...
- Comment gérer le fait de travailler à plusieurs sur le même dépôt et sur les mêmes fichiers ? (cf. Astuces en fin de TP)
- Comment se comporte le "merge" si l'un de vos collègues a déjà fusionné sa branche `doc/contrib_my_username` dans la branche `develop` ?
- Une fois le "merge" effectué, vérifiez l'existence du fichier dans la branche `develop`.

**Note: Pour créer la merge request, vous auriez aussi simplement pu accéder à l'URL renvoyée par GitLab au moment du `git push`, dans votre terminal.**

- Vous pouvez maintenant, et à tout moment, régénérer la documentation en HTML :

  ```bash
  cd docs
  make html
  ```

- Visualisez le fichier `build/html/index.html` dans votre navigateur et appréciez le résultat :cocktail:.

### Bonus: patch & diff

1. Lorsque vous ajoutez des changements à l'index, le mode "patch" permet d'avoir une granularité plus fine, de façon interactive, et de ne sélectionner qu'une partie des modifications des fichiers, au niveau de la ligne voire même du caractère :

  ```bash
  git add -p
  ```

2. Lorsque des modifications sont apportées aux fichiers versionnés par git, la commande `git diff [--cached]` permet de les afficher, qu'elles soient déjà indexées (avec l'option `--cached`) ou non.

---

## Étape 3: Personnaliser la documentation

**Note: n'oubliez pas de créer des branches et de versionner vos modifications.**

Dans un premier temps, nous allons configurer Sphinx pour qu'il génère la documentation en HTML à partir de fichiers au format Markdown (`.md`). Vous devrez vous organiser en équipe pour qu'à la fin de cette étape, chacun d'entre vous puisse générer la documentation en HTML sur sa machine à partir de fichiers Markdown (`.md`) et sur la branche `develop`.

:arrow_forward: Localement sur votre ordinateur :computer:

- Installez le parser Markdown pour Sphinx :

  ```bash
  pip install myst-parser
  ```

- Modifiez le fichier `conf.py` dans le répertoire `docs` pour y inclure le module `myst_parser` :

  ```python
  extensions = [
      # other extensions ...
      'myst_parser',
      # ...
  ]
  ```

- Ajustez la configuration de Sphinx pour qu'il génère la documentation à partir de fichiers Markdown :

  ```python
  source_suffix = {
      '.rst': 'restructuredtext',
      '.txt': 'markdown',
      '.md': 'markdown',
  }
  ```

- Pour plus de détails, vous pouvez consulter la documentation de [MyST](https://myst-parser.readthedocs.io/en/latest/syntax/optional.html).

Maintenant que Sphinx est configuré pour générer la documentation à partir de fichiers Markdown, nous allons pouvoir égayer un peu la documentation en ajoutant un thème personnalisé.

- Installez le thème personnalisé `anf-qualite-theme` depuis le dépôt GitLab de l'ANF Qualité :

  ```bash
  pip install anf-qualite-theme --index-url https://gitlab.in2p3.fr/api/v4/projects/20854/packages/pypi/simple
  ```

- Modifiez le fichier `conf.py` dans le répertoire `docs` pour y inclure le thème personnalisé :

  ```python
  html_theme = 'anf_qualite_theme'
  ```

- Vérifiez que le thème est bien pris en compte en générant à nouveau la documentation en HTML :

  ```bash
  make html
  ```

---

## Étape 4: Gestion des _issues_ dans GitLab

Comme vous venez de le voir, travailler à plusieurs sur le même projet peut vite devenir compliqué. Pour simplifier le travail collaboratif, GitLab propose un système "d'issues" qui permet de créer des tickets pour chaque tâche à réaliser (ex : correction de bug, amélioration, etc.). Ces tickets peuvent servir à répartir le travail entre les membres de l'équipe, de créer et suivre l'avancement de tâches, de discuter des solutions à mettre en place, etc.

**Note: À réaliser avec votre groupe de recherche** :busts_in_silhouette

:arrow_forward: Sur GitLab :cat:

- Vérifiez au préalable dans `Settings > Repository` que la branche par "défaut" est bien la branche `develop`.
- Dans le projet GitLab, créez un ou plusieurs tickets (issues) qui reprendront les tâches identifiées lors du TP1.
  - Milestone: `Operation Clean Project` (il vous faut trouver comment la créer);
  - Label: documentation, bug, amélioration, ou tout autre label que vous jugerez utile;
  - Temps estimé: `X min` (Regardez du côté des [quick actions](https://gitlab.in2p3.fr/help/user/project/quick_actions));
  - Description: `Corriger le bug...`, `Ajouter la documentation...`, etc.

**Note: L'idée n'est pas d'être exhaustif, mais de vous familiariser avec les issues et de vous faire réfléchir à la manière de découper votre travail. Nous vous encourageons à créer de nouvelles issues au fur et à mesure que vous avancerez dans les TPs.**

---

## Étape 5: Consolider la documentation

En partant du document collaboratif rédigé lors du TP1 et des issues créées dans GitLab, vous allez devoir ajouter de la documentation au projet de recherche. Grâce à la commande `sphinx-quickstart`, vous disposez maintenant d'un squelette de documentation, que vous allez devoir compléter.

:arrow_forward: Sur GitLab :cat:

À partir du ticket correspondant à la documentation que vous allez ajouter, créez une merge request. GitLab créera alors automatiquement une branche sur laquelle vous pourrez travailler.

:arrow_forward: Localement avec Git :computer:

- Placez-vous dans votre répertoire de travail et récupérez la branche créée par GitLab :

  ```bash
  git switch name_of_the_branch
  git pull
  ```

:arrow_forward: Localement via votre éditeur de texte :computer:

- Créez et éditez les fichiers de la documentation.
- Demandez à l'un de vos collègues de jouer le rôle de relecteur. Assignez-lui le statut de "reviewer" dans la merge request et demandez-lui de relire votre documentation, d'ajouter des commentaires au besoin et de valider la merge request.
- Générez régulièrement la documentation en HTML pour vérifier que tout se passe bien :

  ```bash
  make html
  ```

Voici le texte corrigé :

<details>
  <summary>Début de solution</summary>
  À la fin de cette étape, vous pourriez avoir une documentation qui contient les éléments suivants :

  - README.md qui décrit le projet et comment l'installer.
  - Document de spécification qui décrit les fonctionnalités et les objectifs du projet.
  - Document de conception qui décrit les choix algorithmiques, techniques, etc., pour atteindre les objectifs du projet.
  - Plan d'assurance qualité qui décrit les mesures mises en place pour garantir la qualité du code : quels normes ? procédures ? responsabilités ? risques (humains, matériels, dépendances, etc.).
  - Guide du développeur : Installation de l'environnement de développement.
  - Guide de l'utilisateur : Installation du package via le dépôt Git et utilisation de la CLI et des fonctions.
  - Plan de vérification/validation des tests : Quels tests ? Quelles spécifications vérifiées ? Comment ? Quand ? Avec quelles ressources (humaines, matérielles) ?

  :warning: Attention à la gestion des conflits dans la documentation rédigée à plusieurs.
</details>

---

## Étape 6: Utilisation des `docstrings` pour compléter la documentation

Python permet d'ajouter de la documentation directement dans le code, via les `docstrings`. Ces derniers sont des chaînes de caractères qui sont associées à des objets (modules, classes, fonctions, méthodes, etc.) et qui peuvent être récupérées via la fonction `help()`, via l'attribut `__doc__` ou encore via des outils de documentation comme Sphinx.

:arrow_forward: Localement avec Git :computer:

- Ajoutez des `docstrings` aux fonctions du fichier `karma_analysis.py`.
- Configurez Sphinx pour qu'il génère la documentation à partir des `docstrings` du code grâce au module `sphinx.ext.autodoc` (cf. [la documentation](https://www.sphinx-doc.org/en/master/tutorial/automatic-doc-generation.html)).

---

### Étape 7 : Génération automatique de la documentation

Maintenant que vous avez une documentation complète et structurée, vous allez vouloir la partager avec vos collaborateurs et utilisateurs. Pour cela, vous allez demander à GitLab de générer automatiquement la documentation à chaque fois que vous poussez une modification sur la branche `develop` du repo.

Pour cela, nous allons utiliser les "GitLab CI/CD pipelines". Nous verrons plus en détail comment fonctionne un pipeline dans le TP4, mais pour l'instant, retenez que c'est un ensemble d'étapes qui sont exécutées automatiquement à chaque fois que vous poussez une modification sur le repo. Ces étapes peuvent être de la compilation, des tests, de la documentation, etc. Elles sont définies dans un fichier `.gitlab-ci.yml` à la racine du projet.

:arrow_forward: Sur votre ordinateur :computer:

- Créez un fichier `.gitlab-ci.yml` à la racine du projet :

  ```bash
  touch .gitlab-ci.yml
  ```

- Ajoutez le contenu suivant au fichier `.gitlab-ci.yml`:

  ```yaml
  image: python:3
  stages:
    - deploy

  pages:
    stage: deploy
    before_script:
    - pip install sphinx -U
    - pip install myst-parser
    - pip install anf-qualite-theme --index-url https://gitlab.in2p3.fr/api/v4/projects/20854/packages/pypi/simple
    script:
    - cd docs
    - sphinx-build -b html source ../public
    artifacts:
      paths:
      - public
    only:
    - develop
  ```

- Ajoutez et committez le fichier `.gitlab-ci.yml`:

  ```bash
  git add .gitlab-ci.yml
  git commit -m 'Add .gitlab-ci.yml'
  ```

- Poussez les changements de cette branche vers le repo distant sur GitLab :

  ```bash
  git push
  ```

:arrow_forward: Sur GitLab :cat:

- Vérifiez que le job de génération de la documentation a bien été exécuté en naviguant jusqu'à la liste des pipelines (`CI/CD > Pipelines`) de votre projet `groupe_X/karma_analysis`.
- Vérifiez que la documentation est bien disponible en ligne en naviguant jusqu'à `https://ri3.pages.in2p3.fr/ecole-info/2023/anf-qualite-logicielle-tp/groupe_X/karma_analysis/` (remplacez `groupe_X` par le nom de votre groupe).

---

## Quelques astuces et commandes utiles

### Gestion et résolution des conflits

Si vous travaillez en même temps sur le même fichier, il est possible que vous ayez des conflits lors de la fusion de vos branches. C'est-à-dire que Git ne saura pas quelle version du fichier il doit conserver. Il vous faudra alors résoudre le conflit manuellement. Prenons l'exemple de deux utilisateurs...

#### :man_dancing: Utilisateur 1

:arrow_forward: Localement avec Git :computer:

- Placez-vous sur la branche `develop` (créez-la si elle n'existe pas déjà).
- Créez un fichier `fichier_conflit.md` avec le contenu de votre choix.
- Référencez ce fichier dans l'index local et poussez la branche `develop` sur le repo distant :

  ```bash
  git add .
  git commit -m "Ajout fichier_conflit.md"
  git push -u origin develop
  ```

- Créez une branche `feature_1` à partir de `develop` :

  ```bash
  git switch -c feature_1
  ```

- Dans le `fichier_conflit.md` de cette branche, modifiez à votre guise le contenu existant du fichier, créez un commit, puis envoyez les modifications de cette branche sur GitLab.

#### :dancer: Utilisateur 2

:arrow_forward: Localement avec Git :computer:

- Placez-vous sur la branche `develop` :

  ```bash
  git switch develop
  ```

- Créez une branche nommée `feature_2` à partir de `develop`.
- Dans le fichier `fichier_conflit.md` de cette branche, modifiez également le contenu du fichier, différemment de votre binôme dans sa branche `feature_1`, créez un commit, puis envoyez les modifications de cette branche sur GitLab.
- Replacez-vous sur la branche `develop`.

#### :man_dancing: Utilisateur 1

:arrow_forward: Localement avec Git :computer:

- Fusionnez `feature_1` dans `develop` :

  ```bash
  git merge feature_1
  ```

#### :dancer: Utilisateur 2

:arrow_forward: Localement avec Git :computer:

- Fusionnez de la même manière `feature_2` dans `develop`.
  À cet instant, vous devriez obtenir un conflit :

  ```bash
  Auto-merging fichier_conflit.md
  CONFLICT (content): Merge conflict in fichier_conflit.md
  Automatic merge failed; fix conflicts and then commit the result.
  ```

  Voici ce que devrait répondre `git status` :

  ```bash
  On branch develop
  You have unmerged paths.
    (fix conflicts and run "git commit")
    (use "git merge --abort" to abort the merge)

  Unmerged paths:
    (use "git add file..." to mark resolution)
          both modified:   fichier_conflit.md
  ```

### La commande `git stash`

:arrow_forward: Localement avec Git :computer:

- Modifiez l'un des fichiers de votre repo, mais au lieu de commiter vos modifications, rangez-les dans le coffre :

  ```bash
  git stash
  ```

- Passez sur une autre branche et appliquez la commande suivante :

  ```bash
  git stash pop
  ```

- Déterminez ce qu'il s'est passé, et quel est l'avantage de cette méthode sur le cherry picking?

<details>
  <summary>Début d'explication</summary>

  La commande `git stash` permet de stocker temporairement les modifications d'un fichier et de les récupérer plus tard, sans avoir à les commiter. Cela permet de travailler sur plusieurs branches en même temps, sans avoir à commiter des modifications qui ne sont pas encore prêtes à l'être.

</details>

### Visualisation de l'historique d'une branche : `git log`

:arrow_forward: Localement avec Git :computer:

- Exécutez les commandes suivantes :

  ```bash
  git log -p -2 --oneline
  git log --oneline --graph
  git log --graph --decorate --all
  ```

- Quelles sont les différences ?

### Aliases de commandes

:arrow_forward: Localement avec Git :computer:

- Créez un alias pour améliorer la lisibilité de l'historique :

  ```bash
  git config --global alias.fancylog "log --decorate --graph --all"
  git config --global alias.hist "log --pretty=format:\"%h %ad | %s%d [%an]\" --graph --date=short"
  ```

- Exécutez `git fancylog`

---

## Ressources et liens utiles

- Documentation Git : [https://git-scm.com/doc](https://git-scm.com/doc)
- Tutoriel interactif Git : [https://learngitbranching.js.org/](https://learngitbranching.js.org/)
- Documentation Sphinx : [https://www.sphinx-doc.org/en/master/](https://www.sphinx-doc.org/en/master/)


N'hésitez pas à explorer ces ressources pour en apprendre plus sur Git et Sphinx.
