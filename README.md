# :sparkle: ANF QUALITE LOGICIELLE 2023 :sparkle:

- [Référence Indico de l'ANF](https://indico.in2p3.fr/event/29229/timetable/#20231015)

# Jour 1
- Introduction  [![](/ressources/presentations/images/slides.png)](https://ri3.pages.in2p3.fr/ecole-info/2023/anf-qualite-logicielle/ressources/presentations/slides/introduction.html)
- Principes de bases de la qualité logicielle [![](/ressources/presentations/images/pdf.png)](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/principes_bases.pdf?inline=false)
- Normes [![](/ressources/presentations/images/pdf.png)](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/normes.pdf?inline=false)
- Introduction à l’intégration continue [![](/ressources/presentations/images/slides.png)](https://ri3.pages.in2p3.fr/ecole-info/2023/anf-qualite-logicielle/ressources/presentations/slides/introduction_ci.html)  [![](/ressources/presentations/images/pdf.png)](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/introduction_ci.pdf?inline=false)
- [TP : Généralités sur la qualité logicielle](TP1.md)

Ressources :

- [RNC-CNES-E-ST-40-110](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/RNC-CNES-E-ST-40-110.pdf?ref_type=heads&inline=false)
- [RNC-CNES-Q-ST-80-100](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/RNC-CNES-Q-ST-80-100.pdf?ref_type=heads&inline=false) 
- [Recommended Documentation In The Case Of A Standard Software Development pg 23](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/RNC-CNES-Q-ST-80-100_v2008.pdf?ref_type=heads&inline=false) 


# Jour 2
- Les outils collaboratifs [![](/ressources/presentations/images/slides.png)](https://ri3.pages.in2p3.fr/ecole-info/2023/anf-qualite-logicielle/ressources/presentations/slides/outils_collaboratifs.html)
 [![](/ressources/presentations/images/pdf.png)](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/outils_collaboratifs.pdf?inline=false)
- Git [![](/ressources/presentations/images/slides.png)](https://ri3.pages.in2p3.fr/ecole-info/2023/anf-qualite-logicielle/ressources/presentations/slides/git.html) [![](/ressources/presentations/images/pdf.png)](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/git.pdf?inline=false)
- Gitlab [![](/ressources/presentations/images/slides.png)](https://ri3.pages.in2p3.fr/ecole-info/2023/anf-qualite-logicielle/ressources/presentations/slides/gitlab.html) [![](/ressources/presentations/images/pdf.png)](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/gitlab.pdf?inline=false)
- Documentation [![](/ressources/presentations/images/slides.png)](https://ri3.pages.in2p3.fr/ecole-info/2023/anf-qualite-logicielle/ressources/presentations/slides/documentation.html) [![](/ressources/presentations/images/pdf.png)](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/documentation.pdf?inline=false)
- [TP : Manipulation d'un dépôt Git et Gitlab](TP2.md)

# Jour 3
- Les tests [![](/ressources/presentations/images/slides.png)](https://ri3.pages.in2p3.fr/ecole-info/2023/anf-qualite-logicielle/ressources/presentations/slides/outils_test.html) [![](/ressources/presentations/images/pdf.png)](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/outils_test.pdf?inline=false)
- Outils d'analyse et de mesure de la qualité du code [![](/ressources/presentations/images/slides.png)](https://ri3.pages.in2p3.fr/ecole-info/2023/anf-qualite-logicielle/ressources/presentations/slides/outils_analyse.html) [![](/ressources/presentations/images/pdf.png)](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/outils_analyse.pdf?inline=false)
- Securité
  - Présentation générale : [![](/ressources/presentations/images/pdf.png)](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/securite.pdf?inline=false)
  - Attaque par la chaine d'approvisionnement [![](/ressources/presentations/images/slides.png)](https://sonny.lion.pages.in2p3.fr/anf-qualite/1) [![](/ressources/presentations/images/pdf.png)](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/supply-chain-attack.pdf?inline=false)
- [TP : Automatisation des tests, de la documentation et mesures de la qualité du code](TP3.md)

Ressources : [Cookbook pour les mocks](ressources/mock_cookbook.md)
# Jour 4 
- L'usine logicielle [![](/ressources/presentations/images/slides.png)](https://ri3.pages.in2p3.fr/ecole-info/2023/anf-qualite-logicielle/ressources/presentations/slides/usine_logicielle.html) [![](/ressources/presentations/images/pdf.png)](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/usine_logicielle.pdf?inline=false)
- Gitlab CI [![](/ressources/presentations/images/slides.png)](https://ri3.pages.in2p3.fr/ecole-info/2023/anf-qualite-logicielle/ressources/presentations/slides/outils_cicd.html) [![](/ressources/presentations/images/pdf.png)](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/outils_cicd.pdf?inline=false)
- [TP Gitlab CI](TP4.md)


# Jour 5
- Bonnes pratiques et mise en oeuvre [![](/ressources/presentations/images/slides.png)](https://ri3.pages.in2p3.fr/ecole-info/2023/anf-qualite-logicielle/ressources/presentations/slides/bonnes_pratiques.html) [![](/ressources/presentations/images/pdf.png)](https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/bonnes_pratiques.pdf?inline=false)
- Conclusion
