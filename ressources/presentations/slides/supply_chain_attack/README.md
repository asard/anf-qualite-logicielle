
# ANF "Qualité logicielle +" (15-20 octobre 2023) - Présentation Slidev

Ce dépôt contient les slides réalisées avec le package Slidev pour l'ANF (Actions nationales de formation) "Qualité logicielle +" qui se tiendra du 15 au 20 octobre 2023. La présentation compilée est disponible [ici](https://sonny.lion.pages.in2p3.fr/anf-qualite).

Note: l'avertissement de sécurité est lié à l'utilisation d'un sous-sous-domaine de pages.in2p3.fr. Il n'y a aucun risque à visiter ce site.

## Liens utiles

-   Slidev : [GitHub](https://github.com/slidevjs/slidev)
-   Documentation Slidev : [sli.dev](https://sli.dev/)

## Installation en local

Pour installer cette présentation en local et l'exécuter sur votre propre machine, vous devez avoir Node.js et Yarn installés au préalable. Une fois que vous avez satisfait à ces exigences, suivez les étapes ci-dessous :

1.  Clonez ce dépôt :

```
git clone git@gitlab.in2p3.fr:sonny.lion/anf-qualite.git
```

2.  Accédez au répertoire :


```
cd anf-qualite
```

3.  Installez les dépendances à l'aide de Yarn :

```
yarn
```

4.  Exécutez la présentation en mode développement :

```
yarn dev
```

## Modification des slides

Pour éditer le contenu de la présentation, vous pouvez simplement modifier le fichier `slides.md` dans le répertoire du projet. Les modifications apportées à ce fichier seront automatiquement rechargées dans le navigateur lorsque vous exécuterez la commande `yarn dev`.

## Publication de la présentation

Une fois que vous avez terminé d'éditer la présentation et que vous êtes prêt à la publier, vous pouvez ompilez la présentation en utilisant la commande Yarn :

```
yarn run build
``` 

Cette commande générera un répertoire `dist` contenant les fichiers HTML, CSS et JavaScript de la présentation.

## Déploiement Continu

Le déploiement automatique de la présentation est assuré par GitLab CI. Le fichier `.gitlab-ci.yml` contient les instructions pour compiler la présentation et la déployer sur le serveur Pages de l'IN2P3. La présentation est accessible à l'adresse suivante : [https://sonny.lion.pages.in2p3.fr/anf-qualite](https://sonny.lion.pages.in2p3.fr/anf-qualite)
