---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Bundler ^kSHbprt4

"Mon"
application ^2K4axrwW

"Mon" code ^PoVUfkX3

HTML ^UlStjjzH

CSS ^Pd4fl5dV

JS ^F2nPARo7


# Embedded files
07e817d88a8ef371b2a23fea9f97e70a6246bc53: [[Pasted Image 20230719091550_792.png]]

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.6",
	"elements": [
		{
			"type": "rectangle",
			"version": 156,
			"versionNonce": 92930109,
			"isDeleted": false,
			"id": "jFqMhaFgaesZ9FcnqNedX",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "dashed",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -242.03553253878366,
			"y": -209.79269915599505,
			"strokeColor": "#f08c00",
			"backgroundColor": "#ffec99",
			"width": 132.18508304259439,
			"height": 273.66574738334964,
			"seed": 832958355,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"id": "p1Q9bP2qKVDEaPNNziGii",
					"type": "arrow"
				}
			],
			"updated": 1689751321198,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 281,
			"versionNonce": 927922931,
			"isDeleted": false,
			"id": "tg_4i_vHNRJ0dTYdxU6_x",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -8.499142480037904,
			"y": -111.6691707946959,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffec99",
			"width": 128,
			"height": 75,
			"seed": 1688584627,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "kSHbprt4"
				},
				{
					"id": "p1Q9bP2qKVDEaPNNziGii",
					"type": "arrow"
				},
				{
					"id": "-bfoR8Rdcpw-aJIRhOQYM",
					"type": "arrow"
				}
			],
			"updated": 1689751594501,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 124,
			"versionNonce": 1701355965,
			"isDeleted": false,
			"id": "kSHbprt4",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 19.790888953067565,
			"y": -86.6691707946959,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 71.41993713378906,
			"height": 25,
			"seed": 1547683539,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1689751594501,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Bundler",
			"rawText": "Bundler",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "tg_4i_vHNRJ0dTYdxU6_x",
			"originalText": "Bundler",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "image",
			"version": 453,
			"versionNonce": 982152723,
			"isDeleted": false,
			"id": "czNPJrHxhStGuTlO-0sSy",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 229.13849448726694,
			"y": -114.06816441631449,
			"strokeColor": "transparent",
			"backgroundColor": "transparent",
			"width": 100.43688205543367,
			"height": 100.43688205543367,
			"seed": 1815737565,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "-bfoR8Rdcpw-aJIRhOQYM",
					"type": "arrow"
				}
			],
			"updated": 1689751378060,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "07e817d88a8ef371b2a23fea9f97e70a6246bc53",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "text",
			"version": 136,
			"versionNonce": 813675027,
			"isDeleted": false,
			"id": "2K4axrwW",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 228.26698495346034,
			"y": 14.929475027360297,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 102.17990112304688,
			"height": 50,
			"seed": 1719299251,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1689756363352,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "\"Mon\"\napplication",
			"rawText": "\"Mon\"\napplication",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "\"Mon\"\napplication",
			"lineHeight": 1.25,
			"baseline": 43
		},
		{
			"type": "text",
			"version": 83,
			"versionNonce": 233768499,
			"isDeleted": false,
			"id": "PoVUfkX3",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -226.98242573470952,
			"y": 95.33130461639735,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 102.2999267578125,
			"height": 25,
			"seed": 2094284307,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1689756372745,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "\"Mon\" code",
			"rawText": "\"Mon\" code",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "\"Mon\" code",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "ellipse",
			"version": 388,
			"versionNonce": 678243059,
			"isDeleted": false,
			"id": "zdMEBTyyyarXzoAlXn9ld",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -213.9954457472844,
			"y": -30.33997075068936,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"width": 78,
			"height": 76,
			"seed": 1232960285,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "UlStjjzH"
				}
			],
			"updated": 1689751393909,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 394,
			"versionNonce": 1082487229,
			"isDeleted": false,
			"id": "UlStjjzH",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -196.8326047203957,
			"y": -2.2100284357781703,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"width": 43.519989013671875,
			"height": 20,
			"seed": 230297469,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1689751393909,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "HTML",
			"rawText": "HTML",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "zdMEBTyyyarXzoAlXn9ld",
			"originalText": "HTML",
			"lineHeight": 1.25,
			"baseline": 14
		},
		{
			"type": "ellipse",
			"version": 420,
			"versionNonce": 1506423325,
			"isDeleted": false,
			"id": "0cA_abU-1t6nKhxjWKEYu",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -213.9954457472844,
			"y": -112.8786482110961,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"width": 78,
			"height": 76,
			"seed": 79116595,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "Pd4fl5dV"
				}
			],
			"updated": 1689751397157,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 433,
			"versionNonce": 616516147,
			"isDeleted": false,
			"id": "Pd4fl5dV",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -189.9525998375832,
			"y": -84.74870589618492,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"width": 29.759979248046875,
			"height": 20,
			"seed": 1719638739,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1689751397157,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "CSS",
			"rawText": "CSS",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "0cA_abU-1t6nKhxjWKEYu",
			"originalText": "CSS",
			"lineHeight": 1.25,
			"baseline": 14
		},
		{
			"type": "ellipse",
			"version": 425,
			"versionNonce": 1297976275,
			"isDeleted": false,
			"id": "GJdfQaobbzqWYjdKHvAtT",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -213.9954457472844,
			"y": -196.5803090629841,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"width": 78,
			"height": 76,
			"seed": 1757616787,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "F2nPARo7"
				}
			],
			"updated": 1689751399789,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 443,
			"versionNonce": 1379032563,
			"isDeleted": false,
			"id": "F2nPARo7",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -185.0086072228371,
			"y": -168.4503667480729,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"width": 19.871994018554688,
			"height": 20,
			"seed": 357060659,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1689751580875,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "JS",
			"rawText": "JS",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "GJdfQaobbzqWYjdKHvAtT",
			"originalText": "JS",
			"lineHeight": 1.25,
			"baseline": 14
		},
		{
			"type": "arrow",
			"version": 104,
			"versionNonce": 1163887603,
			"isDeleted": false,
			"id": "p1Q9bP2qKVDEaPNNziGii",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -101.58465722246959,
			"y": -76.19419160532065,
			"strokeColor": "#1971c2",
			"backgroundColor": "#ffec99",
			"width": 86.3434135331702,
			"height": 2.072361538826158,
			"seed": 1630745661,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689756381937,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "jFqMhaFgaesZ9FcnqNedX",
				"gap": 8.265792273719711,
				"focus": -0.036260079334629436
			},
			"endBinding": {
				"elementId": "tg_4i_vHNRJ0dTYdxU6_x",
				"gap": 6.742101209261477,
				"focus": -0.04470860729562497
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					86.3434135331702,
					2.072361538826158
				]
			]
		},
		{
			"type": "arrow",
			"version": 365,
			"versionNonce": 1766886803,
			"isDeleted": false,
			"id": "-bfoR8Rdcpw-aJIRhOQYM",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 131.9050279505007,
			"y": -76.09830555097432,
			"strokeColor": "#1971c2",
			"backgroundColor": "#ffec99",
			"width": 79.30228151616114,
			"height": 0,
			"seed": 1125614515,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689756381937,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "tg_4i_vHNRJ0dTYdxU6_x",
				"gap": 12.404170430538613,
				"focus": -0.05144359350075775
			},
			"endBinding": {
				"elementId": "czNPJrHxhStGuTlO-0sSy",
				"gap": 17.931185020605085,
				"focus": 0.24390606143301743
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					79.30228151616114,
					0
				]
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "#ffec99",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 0,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "center",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 264.32373499584406,
		"scrollY": 610.3189325639164,
		"zoom": {
			"value": 0.8500000000000001
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"currentStrokeOptions": null,
		"previousGridSize": null
	},
	"files": {}
}
```
%%