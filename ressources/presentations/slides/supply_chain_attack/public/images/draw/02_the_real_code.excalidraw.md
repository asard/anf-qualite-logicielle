---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Bundler ^YVJIm5ZX

HTML ^cf9cEZp5

CSS ^vPkB1xl6

JS ^A72IgY4e


# Embedded files
07e817d88a8ef371b2a23fea9f97e70a6246bc53: [[Pasted Image 20230719091550_792.png]]

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.6",
	"elements": [
		{
			"type": "arrow",
			"version": 257,
			"versionNonce": 86784509,
			"isDeleted": false,
			"id": "J8FuL57BpOXX08n4e8Cdh",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -366.4144801451472,
			"y": -167.00664380993516,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 48.47455899893765,
			"height": 10.393627512721167,
			"seed": 1695688435,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755887809,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "hADKKfyPJzkYGTVHetRtv",
				"focus": -0.28206899430937427,
				"gap": 11.721174588305376
			},
			"endBinding": {
				"elementId": "BOarT-OYgWz-DWPX5IQb3",
				"focus": 0.3932866042720282,
				"gap": 27.316011499507226
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					18.28570050185664,
					-6.26754896723034
				],
				[
					48.47455899893765,
					-10.393627512721167
				]
			]
		},
		{
			"type": "ellipse",
			"version": 360,
			"versionNonce": 634972083,
			"isDeleted": false,
			"id": "ohX_H4xoOMy3OUDR59j1S",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -467.6065598166341,
			"y": -145.03559232740977,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 976436947,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "PMHxQ8acM0gMVjWoQzXVE",
					"type": "arrow"
				},
				{
					"id": "M457dNhYqPpn-FYRHn4dU",
					"type": "arrow"
				},
				{
					"id": "a_AF8YlxznNymcuO3-EJw",
					"type": "arrow"
				}
			],
			"updated": 1689755887170,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 410,
			"versionNonce": 1651693299,
			"isDeleted": false,
			"id": "hADKKfyPJzkYGTVHetRtv",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -405.72418545191226,
			"y": -169.953807896807,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 805826141,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "PMHxQ8acM0gMVjWoQzXVE",
					"type": "arrow"
				},
				{
					"id": "J8FuL57BpOXX08n4e8Cdh",
					"type": "arrow"
				},
				{
					"id": "ehVKXZtI0a7fjJItKe6cN",
					"type": "arrow"
				}
			],
			"updated": 1689755887170,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 414,
			"versionNonce": 1112143411,
			"isDeleted": false,
			"id": "UUHzsH-u54VnqS2oafG6u",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -444.4975413549483,
			"y": -258.3335196509928,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 2058576157,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "eu8HsQWDElDqvdzaCr02b",
					"type": "arrow"
				},
				{
					"id": "nXcJOFU1-kvtOHQXBOfKR",
					"type": "arrow"
				},
				{
					"id": "UPeFkZ-GtVFQs9UnsA_Gf",
					"type": "arrow"
				}
			],
			"updated": 1689755887170,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 417,
			"versionNonce": 466765171,
			"isDeleted": false,
			"id": "fmVH7wUPsyHhGAB1GePZ8",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -382.3078175799877,
			"y": -251.31209922478754,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 49169715,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "nXcJOFU1-kvtOHQXBOfKR",
					"type": "arrow"
				},
				{
					"id": "c7Ik_wZznnatAfiHym3ce",
					"type": "arrow"
				}
			],
			"updated": 1689755887170,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 421,
			"versionNonce": 1248087965,
			"isDeleted": false,
			"id": "msKnxv9O6caVxlFqkrQGA",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -485.62300385129316,
			"y": -319.52018336506677,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 1233552957,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "UPeFkZ-GtVFQs9UnsA_Gf",
					"type": "arrow"
				}
			],
			"updated": 1689755887170,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 410,
			"versionNonce": 1674728445,
			"isDeleted": false,
			"id": "LmSawpcV_64ie2igr5PRB",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -516.7178657387732,
			"y": -252.31515928567399,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 805807645,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "eu8HsQWDElDqvdzaCr02b",
					"type": "arrow"
				}
			],
			"updated": 1689755887170,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 456,
			"versionNonce": 1036045405,
			"isDeleted": false,
			"id": "1oGiycZrojrINKAHzdevl",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -383.2444150637552,
			"y": -13.307896922000339,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 989960851,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "7V63RAvWo_BBMuY7spTgh",
					"type": "arrow"
				},
				{
					"id": "AN_Eg00us9jzVZo618pm3",
					"type": "arrow"
				},
				{
					"id": "m7LrF5IWRiiSx6pFhuGp2",
					"type": "arrow"
				}
			],
			"updated": 1689755887170,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 422,
			"versionNonce": 1010112797,
			"isDeleted": false,
			"id": "mYQf-ZpkbOSXtIDnWAbcG",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -404.3086763423708,
			"y": 35.84204606143615,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 1487332541,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "m7LrF5IWRiiSx6pFhuGp2",
					"type": "arrow"
				}
			],
			"updated": 1689755887170,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 431,
			"versionNonce": 1137569149,
			"isDeleted": false,
			"id": "O5bkMPV3HRwFvpORXF8sK",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -441.4218985951699,
			"y": -8.292596617568023,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 667329779,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "AN_Eg00us9jzVZo618pm3",
					"type": "arrow"
				},
				{
					"id": "umcCiG-gaYecGrsrmuFIR",
					"type": "arrow"
				}
			],
			"updated": 1689755887170,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 445,
			"versionNonce": 2096063091,
			"isDeleted": false,
			"id": "EbqaUIuIiksF-OPED2o7G",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -515.6483431007678,
			"y": 17.786964965479818,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 9734109,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "umcCiG-gaYecGrsrmuFIR",
					"type": "arrow"
				}
			],
			"updated": 1689755887170,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 429,
			"versionNonce": 274766867,
			"isDeleted": false,
			"id": "DGdcRSGRc8z0VZfPHMISw",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -462.1773661118946,
			"y": -84.68142016149224,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 231816957,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "ehVKXZtI0a7fjJItKe6cN",
					"type": "arrow"
				}
			],
			"updated": 1689755887170,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 428,
			"versionNonce": 1085907379,
			"isDeleted": false,
			"id": "7qdpePFmNLW6GlhPPwZxH",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -549.8188477480264,
			"y": -124.92653155309378,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 1107312253,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "M457dNhYqPpn-FYRHn4dU",
					"type": "arrow"
				}
			],
			"updated": 1689755887170,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 409,
			"versionNonce": 924906323,
			"isDeleted": false,
			"id": "ecwVMME12C7elODL55i5b",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -557.8433282351182,
			"y": -59.72762759547388,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 1361177299,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "a_AF8YlxznNymcuO3-EJw",
					"type": "arrow"
				}
			],
			"updated": 1689755887170,
			"link": null,
			"locked": false
		},
		{
			"type": "arrow",
			"version": 139,
			"versionNonce": 653128285,
			"isDeleted": false,
			"id": "PMHxQ8acM0gMVjWoQzXVE",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -430.06212139259304,
			"y": -138.66763820521774,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 19.14262471361775,
			"height": 10.381275269673239,
			"seed": 1117252125,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755887809,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "ohX_H4xoOMy3OUDR59j1S",
				"focus": 0.14082272143844446,
				"gap": 8.6757551739288
			},
			"endBinding": {
				"elementId": "hADKKfyPJzkYGTVHetRtv",
				"focus": 0.34126622141382207,
				"gap": 5.775689682865462
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					19.14262471361775,
					-10.381275269673239
				]
			]
		},
		{
			"type": "arrow",
			"version": 143,
			"versionNonce": 748265149,
			"isDeleted": false,
			"id": "eu8HsQWDElDqvdzaCr02b",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -477.40622273670175,
			"y": -238.94579092800936,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 24.83119116280966,
			"height": 2.4481456076009636,
			"seed": 103815571,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755887809,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "LmSawpcV_64ie2igr5PRB",
				"focus": -0.00870486898553234,
				"gap": 8.69366585544267
			},
			"endBinding": {
				"elementId": "UUHzsH-u54VnqS2oafG6u",
				"focus": 0.07783343196647195,
				"gap": 8.100934257746625
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					24.83119116280966,
					-2.4481456076009636
				]
			]
		},
		{
			"type": "arrow",
			"version": 240,
			"versionNonce": 1825619741,
			"isDeleted": false,
			"id": "nXcJOFU1-kvtOHQXBOfKR",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -402.7377817048725,
			"y": -240.8693339054101,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 11.890992951204566,
			"height": 0.6994701736002753,
			"seed": 1738555443,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755887809,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "UUHzsH-u54VnqS2oafG6u",
				"focus": 0.00273664808863061,
				"gap": 11.063358751142314
			},
			"endBinding": {
				"elementId": "fmVH7wUPsyHhGAB1GePZ8",
				"focus": 0.208862986916563,
				"gap": 8.983341607114173
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					11.890992951204566,
					0.6994701736002753
				]
			]
		},
		{
			"type": "arrow",
			"version": 273,
			"versionNonce": 1845810045,
			"isDeleted": false,
			"id": "c7Ik_wZznnatAfiHym3ce",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -343.52060004712234,
			"y": -235.21547678994636,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 33.44120521511354,
			"height": 9.925783418926926,
			"seed": 539515901,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755887809,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "fmVH7wUPsyHhGAB1GePZ8",
				"focus": -0.20555291771550507,
				"gap": 8.045450047220688
			},
			"endBinding": {
				"elementId": "BOarT-OYgWz-DWPX5IQb3",
				"focus": 0.3415869496947752,
				"gap": 19.455485185306486
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					15.494574136841265,
					2.3321992950085644
				],
				[
					33.44120521511354,
					9.925783418926926
				]
			]
		},
		{
			"type": "arrow",
			"version": 200,
			"versionNonce": 1819761629,
			"isDeleted": false,
			"id": "UPeFkZ-GtVFQs9UnsA_Gf",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -456.9352960440442,
			"y": -283.36770690612104,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 16.925476521765347,
			"height": 18.034842326013916,
			"seed": 1231712957,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755887809,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "msKnxv9O6caVxlFqkrQGA",
				"focus": 0.26726149834113844,
				"gap": 8.548405334984377
			},
			"endBinding": {
				"elementId": "UUHzsH-u54VnqS2oafG6u",
				"focus": 0.49421756307635095,
				"gap": 9.550811224149125
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					16.925476521765347,
					18.034842326013916
				]
			]
		},
		{
			"type": "arrow",
			"version": 215,
			"versionNonce": 540661821,
			"isDeleted": false,
			"id": "M457dNhYqPpn-FYRHn4dU",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -511.5084522585032,
			"y": -119.42545448346397,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 33.97677307467802,
			"height": 6.106378372348331,
			"seed": 617565341,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755887809,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "7qdpePFmNLW6GlhPPwZxH",
				"focus": -0.38769918154789507,
				"gap": 9.718884570238924
			},
			"endBinding": {
				"elementId": "ohX_H4xoOMy3OUDR59j1S",
				"focus": 0.05656021068190834,
				"gap": 10.175162829810642
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					33.97677307467802,
					-6.106378372348331
				]
			]
		},
		{
			"type": "arrow",
			"version": 257,
			"versionNonce": 1127073949,
			"isDeleted": false,
			"id": "a_AF8YlxznNymcuO3-EJw",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -519.5155697224848,
			"y": -54.11562853578178,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 54.09705328078837,
			"height": 52.29156702728352,
			"seed": 1447615293,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755887809,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "ecwVMME12C7elODL55i5b",
				"focus": 0.5489582663341891,
				"gap": 9.69083492679121
			},
			"endBinding": {
				"elementId": "ohX_H4xoOMy3OUDR59j1S",
				"focus": -0.4607680779265981,
				"gap": 10.56091669976973
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					54.09705328078837,
					-52.29156702728352
				]
			]
		},
		{
			"type": "arrow",
			"version": 365,
			"versionNonce": 1459924221,
			"isDeleted": false,
			"id": "ehVKXZtI0a7fjJItKe6cN",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -431.2607042729551,
			"y": -89.55475844427167,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 29.86126121433722,
			"height": 41.316868733041446,
			"seed": 1024323315,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755887810,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "DGdcRSGRc8z0VZfPHMISw",
				"focus": 0.028906623736218058,
				"gap": 10.233479948111256
			},
			"endBinding": {
				"elementId": "hADKKfyPJzkYGTVHetRtv",
				"focus": -0.2988138011987626,
				"gap": 9.93317536938553
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					29.86126121433722,
					-41.316868733041446
				]
			]
		},
		{
			"type": "arrow",
			"version": 479,
			"versionNonce": 725615965,
			"isDeleted": false,
			"id": "7V63RAvWo_BBMuY7spTgh",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -346.6758541486069,
			"y": -16.332453460025448,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 34.59916750978704,
			"height": 29.986609883945178,
			"seed": 323686365,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755887810,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "1oGiycZrojrINKAHzdevl",
				"focus": -0.2002512735875584,
				"gap": 12.805671300091364
			},
			"endBinding": {
				"elementId": "BOarT-OYgWz-DWPX5IQb3",
				"focus": 0.0009999768782214945,
				"gap": 21.45277699211755
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					15.215733317331512,
					-10.79411161887954
				],
				[
					34.59916750978704,
					-29.986609883945178
				]
			]
		},
		{
			"type": "arrow",
			"version": 535,
			"versionNonce": 590434749,
			"isDeleted": false,
			"id": "AN_Eg00us9jzVZo618pm3",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -404.3643235110866,
			"y": 4.685619741910195,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 12.828209386849323,
			"height": 4.077059104490914,
			"seed": 1194954141,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755887810,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "O5bkMPV3HRwFvpORXF8sK",
				"focus": 0.2410390350070915,
				"gap": 6.49806113293582
			},
			"endBinding": {
				"elementId": "1oGiycZrojrINKAHzdevl",
				"focus": 0.5705197999603815,
				"gap": 8.36879731774283
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					12.828209386849323,
					-4.077059104490914
				]
			]
		},
		{
			"type": "arrow",
			"version": 617,
			"versionNonce": 1006244381,
			"isDeleted": false,
			"id": "m7LrF5IWRiiSx6pFhuGp2",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -380.5459887679277,
			"y": 30.51886722267939,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 4.395189337957049,
			"height": 8.179609398546617,
			"seed": 1198688211,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755887810,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "mYQf-ZpkbOSXtIDnWAbcG",
				"focus": -0.17042492691960076,
				"gap": 6.99307488909896
			},
			"endBinding": {
				"elementId": "1oGiycZrojrINKAHzdevl",
				"focus": -0.1336899437655754,
				"gap": 5.648686442002086
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					4.395189337957049,
					-8.179609398546617
				]
			]
		},
		{
			"type": "arrow",
			"version": 616,
			"versionNonce": 1384892029,
			"isDeleted": false,
			"id": "umcCiG-gaYecGrsrmuFIR",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -477.2581838530742,
			"y": 24.05910180494938,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 26.817074452901238,
			"height": 6.991405993251718,
			"seed": 1194875741,
			"groupIds": [
				"umyIvAhGNh2PQTfA480nk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755887810,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "EbqaUIuIiksF-OPED2o7G",
				"focus": -0.21966294676137121,
				"gap": 9.49542311879144
			},
			"endBinding": {
				"elementId": "O5bkMPV3HRwFvpORXF8sK",
				"focus": -0.19142879218078637,
				"gap": 10.736913584005665
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					26.817074452901238,
					-6.991405993251718
				]
			]
		},
		{
			"type": "ellipse",
			"version": 437,
			"versionNonce": 2123298845,
			"isDeleted": false,
			"id": "mFM_CN5N2nKoM_K7zebb4",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -48.19619854460427,
			"y": -39.545795982807505,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 1242645299,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "42b-tPSH-TnrQRxbKPIDx",
					"type": "arrow"
				},
				{
					"id": "RC58q7VPxNSFIZFV5GWIX",
					"type": "arrow"
				},
				{
					"id": "riRAP2xUt8maUfmBDI0QV",
					"type": "arrow"
				},
				{
					"id": "SD5VzFI6ZoZmNnppXLcAQ",
					"type": "arrow"
				}
			],
			"updated": 1689755877090,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 443,
			"versionNonce": 1287345373,
			"isDeleted": false,
			"id": "sqmGh4LjwDgekk5drGDKa",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -17.93962612419682,
			"y": 9.628638347636496,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 1293214589,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "riRAP2xUt8maUfmBDI0QV",
					"type": "arrow"
				},
				{
					"id": "fD3UnaEoUyK9yRKS7oFiG",
					"type": "arrow"
				},
				{
					"id": "t3Gr90whg48roTQYjINFG",
					"type": "arrow"
				},
				{
					"id": "SD5VzFI6ZoZmNnppXLcAQ",
					"type": "arrow"
				}
			],
			"updated": 1689755877090,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 434,
			"versionNonce": 1196805395,
			"isDeleted": false,
			"id": "Bx5RB_eAHs25HHtXDJBPC",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -25.88217217401629,
			"y": 64.70645527556448,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 1562123069,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "t3Gr90whg48roTQYjINFG",
					"type": "arrow"
				}
			],
			"updated": 1689755877090,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 433,
			"versionNonce": 601928371,
			"isDeleted": false,
			"id": "54i47i8HrgBY54bxb4qu1",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 28.523004062494635,
			"y": 66.20113842187479,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 1838925757,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "fD3UnaEoUyK9yRKS7oFiG",
					"type": "arrow"
				}
			],
			"updated": 1689755877090,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 429,
			"versionNonce": 624813139,
			"isDeleted": false,
			"id": "bQcamJg2vT4-q08suaayY",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 59.810213957449236,
			"y": -4.438693851781437,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 1310085459,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "swLorOo3hfMJvl0mXBagc",
					"type": "arrow"
				},
				{
					"id": "8bkJA_WcDHBhRPaoHNGkd",
					"type": "arrow"
				}
			],
			"updated": 1689755877090,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 417,
			"versionNonce": 484027069,
			"isDeleted": false,
			"id": "C6sWnDgIR_zY0oHzA8bXT",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 96.2444502230457,
			"y": 46.717369253427904,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 932859091,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "8bkJA_WcDHBhRPaoHNGkd",
					"type": "arrow"
				}
			],
			"updated": 1689755877090,
			"link": null,
			"locked": false
		},
		{
			"type": "ellipse",
			"version": 446,
			"versionNonce": 1898924829,
			"isDeleted": false,
			"id": "jDn0F_7z85XZnjvgPAt5h",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 31.60671606662831,
			"y": -54.83705389031644,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 1223853555,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "RC58q7VPxNSFIZFV5GWIX",
					"type": "arrow"
				},
				{
					"id": "swLorOo3hfMJvl0mXBagc",
					"type": "arrow"
				},
				{
					"id": "fD3UnaEoUyK9yRKS7oFiG",
					"type": "arrow"
				}
			],
			"updated": 1689755877090,
			"link": null,
			"locked": false
		},
		{
			"type": "arrow",
			"version": 662,
			"versionNonce": 597931453,
			"isDeleted": false,
			"id": "42b-tPSH-TnrQRxbKPIDx",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -31.704743713703408,
			"y": -49.32081130776754,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 15.01941365311859,
			"height": 31.787372753375877,
			"seed": 1167750931,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755923411,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "mFM_CN5N2nKoM_K7zebb4",
				"focus": -0.32946023648161227,
				"gap": 9.800432238322678
			},
			"endBinding": {
				"elementId": "m22xe-K5-eFK-r57y3K3f",
				"gap": 15.400910940703938,
				"focus": -0.12342529780939561
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					3.49674406998966,
					-14.127687348394105
				],
				[
					15.01941365311859,
					-31.787372753375877
				]
			]
		},
		{
			"type": "arrow",
			"version": 842,
			"versionNonce": 568938643,
			"isDeleted": false,
			"id": "RC58q7VPxNSFIZFV5GWIX",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 44.63309513266317,
			"y": -62.310914766199446,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 25.2058070676345,
			"height": 18.789094016349736,
			"seed": 691975315,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755923411,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "jDn0F_7z85XZnjvgPAt5h",
				"focus": 0.8947762977683732,
				"gap": 7.596459680930224
			},
			"endBinding": {
				"elementId": "m22xe-K5-eFK-r57y3K3f",
				"gap": 15.40908621929816,
				"focus": 0.6266406036672711
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-8.669986590643813,
					-9.546955629403882
				],
				[
					-25.2058070676345,
					-18.789094016349736
				]
			]
		},
		{
			"type": "arrow",
			"version": 763,
			"versionNonce": 190668755,
			"isDeleted": false,
			"id": "riRAP2xUt8maUfmBDI0QV",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -15.40130956958005,
			"y": 8.110761212903679,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 8.116312657430711,
			"height": 12.592000525261408,
			"seed": 1667009939,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755877737,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "sqmGh4LjwDgekk5drGDKa",
				"focus": -0.08809671043278032,
				"gap": 5.919835688390219
			},
			"endBinding": {
				"elementId": "mFM_CN5N2nKoM_K7zebb4",
				"focus": 0.1660538249579271,
				"gap": 5.561970158082238
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-8.116312657430711,
					-12.592000525261408
				]
			]
		},
		{
			"type": "arrow",
			"version": 973,
			"versionNonce": 250080627,
			"isDeleted": false,
			"id": "fD3UnaEoUyK9yRKS7oFiG",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 44.393643657377005,
			"y": 55.18848965441113,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 4.336650083744421,
			"height": 71.48720265253905,
			"seed": 1356951443,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755877737,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "54i47i8HrgBY54bxb4qu1",
				"focus": 0.138292645506839,
				"gap": 11.017459084121745
			},
			"endBinding": {
				"elementId": "jDn0F_7z85XZnjvgPAt5h",
				"focus": 0.5386718713254042,
				"gap": 7.87786130084028
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-4.336650083744421,
					-71.48720265253905
				]
			]
		},
		{
			"type": "arrow",
			"version": 1021,
			"versionNonce": 152028947,
			"isDeleted": false,
			"id": "t3Gr90whg48roTQYjINFG",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -9.012027246885694,
			"y": 58.23489369134961,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 2.318356046593017,
			"height": 12.421771523448143,
			"seed": 27079197,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755877737,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "Bx5RB_eAHs25HHtXDJBPC",
				"focus": -0.17060870712342804,
				"gap": 6.524069789588456
			},
			"endBinding": {
				"elementId": "sqmGh4LjwDgekk5drGDKa",
				"focus": 0.02131994214823685,
				"gap": 4.8825592482815985
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					2.318356046593017,
					-12.421771523448143
				]
			]
		},
		{
			"type": "arrow",
			"version": 1168,
			"versionNonce": 1347120307,
			"isDeleted": false,
			"id": "swLorOo3hfMJvl0mXBagc",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 64.55949582519642,
			"y": -5.926930181079893,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 8.94007084870999,
			"height": 13.467158251150604,
			"seed": 1026937907,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755877737,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "bQcamJg2vT4-q08suaayY",
				"focus": 0.04829502773499935,
				"gap": 4.619335405914757
			},
			"endBinding": {
				"elementId": "jDn0F_7z85XZnjvgPAt5h",
				"focus": 0.23360366809472102,
				"gap": 5.611805914109322
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-8.94007084870999,
					-13.467158251150604
				]
			]
		},
		{
			"type": "arrow",
			"version": 1065,
			"versionNonce": 675372627,
			"isDeleted": false,
			"id": "8bkJA_WcDHBhRPaoHNGkd",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 99.50828617711304,
			"y": 45.31593736472618,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 11.331470496259357,
			"height": 15.404034953141526,
			"seed": 1369986877,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755877738,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "C6sWnDgIR_zY0oHzA8bXT",
				"focus": 0.03090078909493673,
				"gap": 5.390090434732858
			},
			"endBinding": {
				"elementId": "bQcamJg2vT4-q08suaayY",
				"focus": 0.031117470568621495,
				"gap": 6.89258806706925
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-11.331470496259357,
					-15.404034953141526
				]
			]
		},
		{
			"type": "ellipse",
			"version": 483,
			"versionNonce": 463872339,
			"isDeleted": false,
			"id": "kIGsCqFuLMcJYgfWIVc4l",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -80.35662703200651,
			"y": 63.56048767958177,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 30.742833343731434,
			"height": 31.73686703170135,
			"seed": 236887037,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"id": "SD5VzFI6ZoZmNnppXLcAQ",
					"type": "arrow"
				}
			],
			"updated": 1689755877090,
			"link": null,
			"locked": false
		},
		{
			"type": "arrow",
			"version": 786,
			"versionNonce": 466099187,
			"isDeleted": false,
			"id": "SD5VzFI6ZoZmNnppXLcAQ",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -56.95606841656418,
			"y": 58.091527787140656,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#b2f2bb",
			"width": 22.244290619352128,
			"height": 59.81048010622841,
			"seed": 364937971,
			"groupIds": [
				"CUc0KL3v4NabWlze2sNxk"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755877738,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "kIGsCqFuLMcJYgfWIVc4l",
				"focus": 0.00567793071296629,
				"gap": 6.992799414422292
			},
			"endBinding": {
				"elementId": "mFM_CN5N2nKoM_K7zebb4",
				"focus": -0.3813822346338837,
				"gap": 6.1746591738480365
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					22.244290619352128,
					-59.81048010622841
				]
			]
		},
		{
			"type": "rectangle",
			"version": 342,
			"versionNonce": 1959207283,
			"isDeleted": false,
			"id": "BOarT-OYgWz-DWPX5IQb3",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "dashed",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -290.6239096467023,
			"y": -269.6326233631465,
			"strokeColor": "#f08c00",
			"backgroundColor": "#ffec99",
			"width": 132.18508304259439,
			"height": 273.66574738334964,
			"seed": 1573780531,
			"groupIds": [
				"FPvfUBAwiHcLRDc9F5UKN"
			],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"id": "xGRb_rj4uGhZ0gtCDZkS2",
					"type": "arrow"
				},
				{
					"id": "J8FuL57BpOXX08n4e8Cdh",
					"type": "arrow"
				},
				{
					"id": "c7Ik_wZznnatAfiHym3ce",
					"type": "arrow"
				},
				{
					"id": "7V63RAvWo_BBMuY7spTgh",
					"type": "arrow"
				}
			],
			"updated": 1689755861505,
			"link": null,
			"locked": false
		},
		{
			"type": "rectangle",
			"version": 466,
			"versionNonce": 116236467,
			"isDeleted": false,
			"id": "m22xe-K5-eFK-r57y3K3f",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -57.08751958795659,
			"y": -171.50909500184736,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffec99",
			"width": 128,
			"height": 75,
			"seed": 365297107,
			"groupIds": [
				"FPvfUBAwiHcLRDc9F5UKN"
			],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "YVJIm5ZX"
				},
				{
					"id": "xGRb_rj4uGhZ0gtCDZkS2",
					"type": "arrow"
				},
				{
					"id": "vcNtcjZfxmc9AT1n_r2qF",
					"type": "arrow"
				},
				{
					"id": "42b-tPSH-TnrQRxbKPIDx",
					"type": "arrow"
				},
				{
					"id": "RC58q7VPxNSFIZFV5GWIX",
					"type": "arrow"
				}
			],
			"updated": 1689755861505,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 307,
			"versionNonce": 270882515,
			"isDeleted": false,
			"id": "YVJIm5ZX",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -28.797488154851123,
			"y": -146.50909500184736,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 71.41993713378906,
			"height": 25,
			"seed": 1049414515,
			"groupIds": [
				"FPvfUBAwiHcLRDc9F5UKN"
			],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1689755860386,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Bundler",
			"rawText": "Bundler",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "m22xe-K5-eFK-r57y3K3f",
			"originalText": "Bundler",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "image",
			"version": 636,
			"versionNonce": 1889432541,
			"isDeleted": false,
			"id": "QUvz0zYrB52161qrlhs9p",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 180.55011737934828,
			"y": -173.90808862346597,
			"strokeColor": "transparent",
			"backgroundColor": "transparent",
			"width": 100.43688205543367,
			"height": 100.43688205543367,
			"seed": 1818813715,
			"groupIds": [
				"FPvfUBAwiHcLRDc9F5UKN"
			],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "vcNtcjZfxmc9AT1n_r2qF",
					"type": "arrow"
				}
			],
			"updated": 1689755860386,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "07e817d88a8ef371b2a23fea9f97e70a6246bc53",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "ellipse",
			"version": 571,
			"versionNonce": 175038621,
			"isDeleted": false,
			"id": "ZM4BTkXIUYudSZaaiSePC",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -262.5838228552031,
			"y": -90.17989495784082,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"width": 78,
			"height": 76,
			"seed": 990771699,
			"groupIds": [
				"FPvfUBAwiHcLRDc9F5UKN"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "cf9cEZp5"
				}
			],
			"updated": 1689755860386,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 579,
			"versionNonce": 1216345629,
			"isDeleted": false,
			"id": "cf9cEZp5",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -245.42098182831438,
			"y": -62.049952642929625,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"width": 43.519989013671875,
			"height": 20,
			"seed": 1304445843,
			"groupIds": [
				"FPvfUBAwiHcLRDc9F5UKN"
			],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1689755923412,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "HTML",
			"rawText": "HTML",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "ZM4BTkXIUYudSZaaiSePC",
			"originalText": "HTML",
			"lineHeight": 1.25,
			"baseline": 14
		},
		{
			"type": "ellipse",
			"version": 603,
			"versionNonce": 1180878077,
			"isDeleted": false,
			"id": "N8q1bMsH9L4MEo-GTFueu",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -262.5838228552031,
			"y": -172.71857241824756,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"width": 78,
			"height": 76,
			"seed": 314071347,
			"groupIds": [
				"FPvfUBAwiHcLRDc9F5UKN"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "vPkB1xl6"
				}
			],
			"updated": 1689755860386,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 617,
			"versionNonce": 1507186003,
			"isDeleted": false,
			"id": "vPkB1xl6",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -238.54097694550188,
			"y": -144.58863010333636,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"width": 29.759979248046875,
			"height": 20,
			"seed": 1465694931,
			"groupIds": [
				"FPvfUBAwiHcLRDc9F5UKN"
			],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1689755860386,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "CSS",
			"rawText": "CSS",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "N8q1bMsH9L4MEo-GTFueu",
			"originalText": "CSS",
			"lineHeight": 1.25,
			"baseline": 14
		},
		{
			"type": "ellipse",
			"version": 608,
			"versionNonce": 924523869,
			"isDeleted": false,
			"id": "1UDwG4iB7kZaojwTriA9Q",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -262.5838228552031,
			"y": -256.42023327013555,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"width": 78,
			"height": 76,
			"seed": 2017716339,
			"groupIds": [
				"FPvfUBAwiHcLRDc9F5UKN"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "A72IgY4e"
				}
			],
			"updated": 1689755860386,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 628,
			"versionNonce": 2016921139,
			"isDeleted": false,
			"id": "A72IgY4e",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -233.5969843307558,
			"y": -228.29029095522435,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"width": 19.871994018554688,
			"height": 20,
			"seed": 2067030547,
			"groupIds": [
				"FPvfUBAwiHcLRDc9F5UKN"
			],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1689755923413,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "JS",
			"rawText": "JS",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "1UDwG4iB7kZaojwTriA9Q",
			"originalText": "JS",
			"lineHeight": 1.25,
			"baseline": 14
		},
		{
			"type": "arrow",
			"version": 653,
			"versionNonce": 1887652179,
			"isDeleted": false,
			"id": "xGRb_rj4uGhZ0gtCDZkS2",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -150.17303433038825,
			"y": -136.0341158124721,
			"strokeColor": "#1971c2",
			"backgroundColor": "#ffec99",
			"width": 86.34341353317018,
			"height": 2.0723615388261862,
			"seed": 1764009907,
			"groupIds": [
				"FPvfUBAwiHcLRDc9F5UKN"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755923411,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "BOarT-OYgWz-DWPX5IQb3",
				"gap": 8.265792273719683,
				"focus": -0.036259923922952644
			},
			"endBinding": {
				"elementId": "m22xe-K5-eFK-r57y3K3f",
				"gap": 6.742101209261477,
				"focus": -0.04470860729562599
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					86.34341353317018,
					2.0723615388261862
				]
			]
		},
		{
			"type": "arrow",
			"version": 914,
			"versionNonce": 2056562419,
			"isDeleted": false,
			"id": "vcNtcjZfxmc9AT1n_r2qF",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 83.31665084258204,
			"y": -135.93822975812577,
			"strokeColor": "#1971c2",
			"backgroundColor": "#ffec99",
			"width": 79.30228151616113,
			"height": 0,
			"seed": 1748711763,
			"groupIds": [
				"FPvfUBAwiHcLRDc9F5UKN"
			],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1689755923411,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "m22xe-K5-eFK-r57y3K3f",
				"gap": 12.404170430538613,
				"focus": -0.051443593500757744
			},
			"endBinding": {
				"elementId": "QUvz0zYrB52161qrlhs9p",
				"gap": 17.931185020605128,
				"focus": 0.24390606143301682
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					79.30228151616113,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 312,
			"versionNonce": 1306131549,
			"isDeleted": true,
			"id": "qwRlde9l",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 179.67860784554168,
			"y": -44.91044917979116,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 102.17990112304688,
			"height": 50,
			"seed": 1724959411,
			"groupIds": [
				"FPvfUBAwiHcLRDc9F5UKN"
			],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1689755930192,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "\"Notre\"\napplication",
			"rawText": "\"Notre\"\napplication",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "\"Notre\"\napplication",
			"lineHeight": 1.25,
			"baseline": 43
		},
		{
			"type": "text",
			"version": 262,
			"versionNonce": 33582909,
			"isDeleted": true,
			"id": "Lyz9HFgj",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -258.3808080306165,
			"y": 35.49138040924589,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 67.91993713378906,
			"height": 50,
			"seed": 1114307667,
			"groupIds": [
				"FPvfUBAwiHcLRDc9F5UKN"
			],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1689755926401,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "\"Notre\"\ncode",
			"rawText": "\"Notre\"\ncode",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "\"Notre\"\ncode",
			"lineHeight": 1.25,
			"baseline": 43
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#2f9e44",
		"currentItemBackgroundColor": "#b2f2bb",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 0,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "center",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": null,
		"scrollX": 601.3747625744412,
		"scrollY": 676.3793242498629,
		"zoom": {
			"value": 0.8500000000000001
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"currentStrokeOptions": null,
		"previousGridSize": null
	},
	"files": {}
}
```
%%