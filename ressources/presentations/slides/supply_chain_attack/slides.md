---
theme: "@web-ijclab/slidev-theme-ijclab"
highlighter: shiki
lineNumbers: false
info: |
  ## Présentation sur le thème de la sécurité informatique
  Attaque par la chaîne d'approvisionnement
  Sonny LION
  Adapté du travail de Jarrod OVERSON
drawings:
  persist: false
title: Attaque par la chaîne d'approvisionnement
showHint: true
---

# Attaque par la chaîne d'approvisionnement
## Sonny LION

Adapté du travail de Jarrod OVERSON


---
layout: default
---

# Une application... du point de vue de son développeur

<img src="/images/draw/01_our_code.excalidraw.svg" class="w-3/5 mt-20 mx-auto">

---
---

# Et une application... en réalité

Il est très facile d'oublier la quantité de code tiers dont dépend notre application...

<img src="/images/draw/02_the_real_code.excalidraw.svg" class="w-4/5 mx-auto">


---

# Et si une partie de ce code est vérolée ?


<img src="/images/draw/03_code_exploit.excalidraw.svg" class="w-4/5 mx-auto mt-10">

---
---

# Cela peut avoir des conséquences désastreuses

Une fuite de données massive et une petite amende de 183 millions de livres (204 millions d'euros)


<img src="/images/001_british_airways.png" class="w-[70%] h-96 mx-auto" style="object-fit: cover;">

---

# Attaque par la chaîne d'approvisionnement : <br>le cas **event-stream**


<img src="/images/002_malicious_code_npm.png" class="w-full h-96 mx-auto mt-10" style="object-fit: cover; object-position: 0 60%;">

---
layout: big-points
---

::points::

- **Que s'est-il passé ?**
- Comment cela a été découvert ?
- Détails de fonctionnement du code malveillant
-  Quelles leçons en tirer ?

---

# Tout commence avec un package npm (JS): **event-stream**


<img src="/images/003_event-stream.png" class="w-full h-96 mx-auto mt-10" style="object-fit: cover; object-position: 0 60%;">

---

# **event-stream** était maintenu par une seule personne, **Dominic Tarr** (qui maintient plus de 700 packages...)

![](/images/004_dominic_tarr.png)



---

# **right9ctrl** a proposé à Dominic de maintenir le package, et a récupéré les droits en septembre 2018

![](/images/005_ownership.png)

---
layout: default
cols: 1-1
---

# Quelques changements innocents...


::left::

```
...b550f5: upgrade dependencies
...37c105: add map and split examples
...477832: remove trailing in split example
...2c2095: better pretty.js example
...a644c5: update readme
```

::right::

**right9ctrl** a commencé par faire quelques changements innocents sur le dépôt pour gagner en crédibilité


---

# Introduction d'une nouvelle dépendance

Le 9 septembre 2018, right9ctrl a introduit une nouvelle dépendance et a publié la version 3.3.6 de event-stream

![](/images/006_new_deps.png)

---

# Arrêt sur image sur un caractère qui peut paraître anodin, l'accent circonflexe !

![](/images/006_new_deps.png)

---
cols: 1-1
---

# Petit rappel sur le Semver (Semantic versioning)

```
Major.Minor.Patch
````

- Major : changement majeur, pas de rétro-compatibilité
- Minor : ajout de fonctionnalités
- Patch : corrections de bugs

::right::


| Symbole | Exemple | Correspondance |
| ------- | ------- | -------------- |
| ^       | ^0.1.0  | 0.\*.\*        |
| ~       | ~0.1.0  | 0.1.\*         |

---

# **right9ctrl** a ensuite supprimé flatmap-stream et a fait passer event-stream en v4.0.0

![](/images/007_v400_event-stream.png)



---
layout: center
class: text-center
---

## Temps total entre le premier commit de right9ctrl et la v4.0.0 : 

# 12 jours

Note : right9ctrl n'a rien fait de mal... pour le moment...

---

# Introduction du code malveillant

Le 5 octobre 2018 (T+31), right9ctrl publie une version malveillante de flatmap-stream@0.1.1

Toutes les nouvelles installations de event-stream@3.3.6 récupèrent flatmap-stream@0.1.1 à cause du **^**

event-stream@3.3.5 était stable depuis plus de 2 ans... **BEAUCOUP** de packages dépendaient de event-stream@ **^** 3.3.5 et allaient basculer sur la version 3.3.6 automatiquement.


---
layout: big-points
---

::points::

- Que s'est-il passé ?
- **Comment le code malveillant a-t-il été découvert ?**
- Détails de fonctionnement du code malveillant
-  Quelles leçons en tirer ?

---

# Le code malveillant utilisait une méthode dépréciée avec node v11.0.0

![](/images/008_method_deprecated.png)



---
layout: default
---

# Node v11.0.0 déployé seulement 18 jours après le package malveillant

![](/images/009_node_v11.png)

---

# D'étranges avertissements apparaissent dans les logs

![](/images/010_deprecation_warning.png)

---

# Le coupable est finalement identifié

Le 20 novembre 2018 (T+77), FallingSnow publie sa trouvaille sur Github

![](/images/011_falling_snow_post.png)


---
layout: center
class: text-center
---


# Comment le code malveillant a-t-il été découvert ?

## Un simple coup de chance.

Si la méthode crypto.createDecipher n'avait pas été dépréciée dans node v11.0.0, qui sait quand la supercherie aurait été découverte ?

---
layout: center
class: text-center
---

## Temps entre le changement de mainteneur et le signalement de FallingSnow sur github ? 

# 77 jours

## Temps pendant lequel flatmap-stream@0.1.1 est resté en activité sans être détecté : 

# 48 jours

Pour un projet qui est téléchargé 1,2 million de fois par semaine...


---
layout: big-points
---

::points::

- Que s'est-il passé ?
- Comment le code malveillant a-t-il été découvert ?
- **Détails de fonctionnement du code malveillant**
-  Quelles leçons en tirer ?

---
layout: default
cols: 1-1
---

# Un peu de rétro-ingénierie...

Flatmap-stream, un code minifié standard

::left::

En version 0.1.0

![](/images/012_flatmap-stream_v0-1-0.png)

::right::

Et en version 0.1.1

![](/images/013_flatmap-stream_v0-1-1.png)

---
layout: default
cols: 1-1
---

# Payload A : le code d'amorçage

::left::

en version *obscurcie*

![](/images/014_payload_a_obfuscated.png)

::right::

et une fois rendu *lisible*

![](/images/015_payload_a_readable.png)

---
layout: default
cols: 1-1
---

# Etape 1 : récupération du code caché

::left::

Derrière `"2e2f746573742f64617461"` se cache le chemin vers un module :

`"./test/data"`

Ce fichier contient à son tour une liste de chaînes de caractères encodées.


::right::


![](/images/017_payload_a_test_data.png)


---
layout: default
cols: 1-1
---

# Etape 2 : compilation d'un nouveau module malveillant 

::left::

![](/images/019_payload_a_final.png)

::right::

- La clé provient d'une description de package -> **quel package ?**
-  `testData[0]` est déchiffré et sert de source pour le module
- Le module compilé exporte ensuite `testData[1]`

---
layout: default
cols: 1-1
---

# Que peut-on en déduire à ce moment là ?

::left::


![](/images/019_payload_a_final.png)

::right::

-  Le script n'atteint son but que si le code s'exécute à partir d'un script `npm`
-  Et uniquement en présence d'un package spécifique (utilisé pour déchiffrer la charge utile suivante)

---

# Comment trouver le package cible ?

La méthode **brute-force**

-  Parcourir l'ensemble des packages présents sur `npm` 
-  Itérer sur les métadonnées de chacun de ces packages
- Tenter de déchiffrer `testData[0]` avec `pkg.description` comme clé
- Exécuter le code déchiffrées comme un script JavaScript 
- En cas de succès, alors jackpot 👍

---
layout: default
---

# Est-ce que cela fonctionne ?

```bash
$ node brute-force-decrypt.js
Password is 'A Secure Bitcoin Wallet' from copay-dash@2.4.0
```

La cible : Copay, le portefeuille Bitcoin sécurisé.

Copay est une plateforme de portefeuille Bitcoin sécurisé pour les ordinateurs de bureau et **les appareils mobiles**.

---
layout: default
cols: 1-1
---

# Payload B : L'injecteur

::left::

![](/images/021_payload_b.png)

::right::

La regex cible uniquement la version release de Copay
```
      npm <command> <script-name> 
argv: [0]    [1]         [2]
```

---


<img src="/images/022_regex_target.png" class="w-4/5 mx-auto">

---
layout: default
---

# Récapitulatif 

L'injecteur :

- ne s'execute que s'il se trouve dans une des phases de construction implicant le package Copay.
- déchiffre C de la même manière que A a déchiffré B. 
- injecte la charge utile C dans un fichier utilisé par **l'application** incluant Copay. 
- La charge utile C sera ensuite exécutée dans l'application mobile/desktop sur le terminal de l'utilisateur.

---
layout: default
cols: 1-1
---

# Payload C : La vraie charge utile

::left::

![](/images/023_payload_c.png)

::right::

La charge utile C en résumé 

- Récupère les clés privés
- Cible les  portefeuilles contenant plus de 100 BTC ou 1000 BCH. 
- Envoie les données à un serveur tiers : `copayapi.host`

---
layout: big-points
---

::points::

- Que s'est-il passé ?
- Comment le code malveillant a-t-il été découvert ?
- Détails de fonctionnement du code malveillant
- **Quelles leçons en tirer ?**

---
layout: center
class: text-center
---

# Cette problématique n'est pas spécifique à node/npm 

Tout dépôt de code **communautaire** est susceptible d'être compromis.


---

# Cela aurait pu être bien pire !

`event-stream` est une dépendance d'outils comme :

-  `azure-cli` (Cloud Azure - Microsoft)
-  l'éditeur `monaco` de Microsoft (brique de base de VSCode)
- et des dizaines d'outils de construction de packages JavaScript (gulp & Cie)

---
layout: default
cols: 1-1
---

::left::

# La bonne nouvelle 

Nous pouvons compter sur la communauté pour :
- enquêter et produire des outils
- limiter l'impact de ces failles
- résoudre les problèmes

Et tout ça, très rapidement !

::right::

# Et la mauvaise...

Cela s'est reproduit à plusieurs reprises depuis...

<img src="/images/024_and_again.png" class="w-4/5 mx-auto"/>


---
cols: 1-1
---

# Et cela se reproduira encore !

::left::

Pour vraiment régler ce problème, il faudrait repenser l'ensemble de l'écosystème :

- les aspects techniques liés aux dépendances et aux gestionnaires de packages (cf. par exemple `Deno` un runtime JavaScript)
- mais aussi les aspects économiques et **humains**

::right::

<img src="/images/025_modern_digital_infra.jpg" class="w-2/3 mx-auto"/>

---

# Que fait le législateur ?

**Cyber Resilience Act** : Renforcement de la cybersécurité des produits numériques en Europe

- Cadre réglementaire européen pour lutter contre les cyberattaques sur les produits connectés.
- S'applique à tous les produits numériques, y compris les logiciels
- Les fabricants doivent intégrer la sécurité dès la conception, fournir des mises à jour et signaler les vulnérabilités/incidents 
- Sanctions en cas de non-respect : amendes jusqu'à 15 millions d'euros ou 2,5% du chiffre d'affaires, et restrictions de commercialisation.
- **Inquiétudes** : un frein au développement des logiciels libres (exemption uniquement pour les logiciels libres **non commerciaux**)


---

# Que pouvez-vous faire en tant que développeur ?

-   Auditez vos dépendances
-   Verrouillez vos dépendances
-   Mettez en cache/vérifiez vos dépendances
-   **Réfléchissez à deux fois avant d'ajouter des dépendances**

---

# Focus : applications monolithique ou micro-services 

Quelle architecture est la plus résiliente ?

Réponse courte : Cela dépend.

|                            | Monolithique | Micro-services                               |
| -------------------------- | ------------ | -------------------------------------------- |
| Surface d'attaque          | réduite      | plus grande mais isolation de chaque service |
| Contrôles de sécurité      | centralisés  | contrôles adaptés à chaque service           |
| Gestion des vulnérabilités | chronophages | mises à jour plus simples et rapides         |
| Niveau d'expertise requis  | moyen        | expert                                       |
