## Normes
- Rajouter des exemples concrets de document
- Mettre en place un template en markdown reprenant la RNC : 
https://gitlab.in2p3.fr/ri3/ecole-info/2023/anf-qualite-logicielle/-/raw/main/ressources/presentations/slides/RNC-CNES-Q-ST-80-100_v2008.pdf?ref_type=heads&inline=false

## Outils collaboratifs 
- Est ce utile de garder cette présentation ?
- Intégrer celà dans la partie usine logicielle ?

# Usine logicielle et Gitlab CI/CD
- voir les présentations de l'anf envol 2023

## Git / Gitlab
- Dérouler un exemple concret Git et Gitlab avec un Merge Request et une revue de Code

## Tests
- Doctest : proposer des cas d'exceptions et des cas plus complets
- supprimer certaines assertions (instanceof)
- Selenium : aller un peu plus loin ou faire une présentation dédiée

## Introduction à l'intégration continue
- passer les cas de déploiement

## SonarQube
- Supprimer l'installation, passer plus de temps sur la présentation du produit
- Git rebase vs git merge : revoir les explications (placement à la fin de la branche ...)

## TP CI/CD
- voir le TP CI/CD de l'anf envol 2023
- Intégrer un outil de sécurité dans la CI/CD
- Rajouter la gestion de packages / gestion de container
- Revoir la partie rules avec le tag (ne fonctionne pas)


