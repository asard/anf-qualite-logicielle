# TP3 - Évaluer et améliorer la qualité du code : test, couverture et respect des normes

Nous allons reprendre le code du TP précédent et l'améliorer progressivement en implémentant des tests unitaires, en utilisant des outils de typage et en vérifiant le respect des normes de codage. Un bon moyen d'ajouter des tests au fil de l'eau est de partir des bugs identifiés pour implémenter des tests qui permettent de les détecter et de les corriger.

**Note : ce TP utilise les mots clés de l'interface de Gitlab dans sa version anglaise. Vous pouvez basculer l'interface de Gitlab en version anglaise, via [les préférences de votre compte utilisateur](https://gitlab.in2p3.fr/-/profile/preferences).**

----

## Étape 0 : Mettre à jour l'environnement de travail

- Installez dans votre environnement les outils `pylint`, `pytest`, `coverage`, `pytest-cov`, `mypy`, `black`, `pre-commit`, `lxml` qui seront utilisés dans ce TP :

  ```bash
  pip install pylint pytest coverage pytest-cov mypy black pre-commit lxml 
  ```

- Observez les différences entre la branche `main` et la branche `tp3`

```bash
git diff main tp3
```
- Mettez à jour votre repository : créez une merge request de la branche tp3 dans la branche main , validez la merge request

----

## Étape 1 : Introduction aux tests unitaires, aux mocks et à la couverture

### Utilisation de `doctest`

Les [doctests](https://docs.python.org/3/library/doctest.html) sont des tests unitaires qui sont inclus directement dans la documentation du code via les `docstrings`. Ils permettent en particulier de vérifier que les exemples fournis dans la documentation sont toujours valides tout en fournissant une manière simple de tester le code.

Pour exécuter les tests `doctest`, il suffit d'exécuter le module `doctest` en ligne de commande :

  ```bash
  python -m doctest src/karma_analysis.py
  ```

ou encore via le package `pytest` :

  ```bash
  pytest --doctest-modules src/karma_analysis.py
  ``` 

 - Ajoutez un [doctest](https://docs.python.org/3/library/doctest.html) à la fonction `linéaire` du script `karma_analysis.py` et vérifiez qu'ils passent.

<details>
<summary>Solution</summary>
<p>
  
  ```python
  def linear_func(x, a, b):
    """Fonction linéaire pour le réglage de la courbe.

    Args:
        x (float): Valeur d'entrée.
        a (float): Coefficient linéaire.
        b (float): Intercept.

    Returns:
        float: Valeur de la fonction linéaire.


    Example:
        >>> linear_func(1, 2, 3)
        5

    """
    return a * x + b

  ```

</p>
</details>

### Utilisation de `pytest`

Le module [pytest](https://docs.pytest.org/en/stable/) permet de créer des tests unitaires de manière simple et efficace en utilisant le mot-clé `assert` et les opérateurs de comparaison au lieu de fonctions spécifiques comme `assertEqual` ou `assertTrue` de la bibliothèque `unittest`.

- Créez un répertoire `tests` et un fichier `test_karma_analysis.py` dans lequel vous allez écrire les tests unitaires pour les fonctions du fichier `karma_analysis.py`.

- Écrivez une fonction `test_quadratic_func` qui teste la fonction `quadratic_func` et vérifiez que le test passe en exécutant la commande :

  ```bash
  pytest tests/test_karma_analysis.py
  ```
- **Utilisation avancée** Utilisez le décorateur [@pytest.mark.parametrize](https://docs.pytest.org/en/7.3.x/how-to/parametrize.html) pour tester la fonction `quadratic_func` avec plusieurs valeurs de paramètres.
- **Utilisation avancée** Utilisez le décorateur `@patch` de la bibliothèque [unittest.mock](https://docs.python.org/3/library/unittest.mock.html) pour simuler l'ouverture d'un fichier de données JSON et tester la fonction `readData`.

- Déterminez la couverture de code en utilisant le module [coverage](https://coverage.readthedocs.io/) via pytest (plugin `pytest-cov`):

  ```bash
  pytest --cov=karma_analysis tests/test_karma_analysis.py
  ``` 

- Ajoutez des tests pour vous rapprocher des 100 % de couverture de code (ou toute autre valeur que vous avez spécifiée dans votre documentation...)


----

## Étape 2 : Typage en Python via `typing` et `mypy`

[mypy](https://mypy.readthedocs.io/en/stable/) est un outil permettant de vérifier la cohérence du typage d'un code Python.

- Exécutez l'outil `mypy` [en ligne de commande](https://mypy.readthedocs.io/en/stable/command_line.html) sur le fichier `karma_analysis.py` :

  ```bash
  mypy src/karma_analysis.py
  ```
- Résolvez les erreurs de typage remontées par `mypy` en utilisant les [annotations de type](https://docs.python.org/3/library/typing.html) et la [documentation](https://mypy.readthedocs.io/en/stable/cheat_sheet_py3.html) de `mypy`.

- Générez la couverture du typage avec `mypy` :

  ```bash
  mypy --html-report type-coverage karma_analysis.py
  ```

- Visualisez le rapport de couverture dans le dossier `type-coverage`

- Typez les fonctions pour obtenir une couverture supérieure à la valeur que vous avez spécifiée dans votre documentation (Avez-vous spécifié cette valeur ?)



## Étape 3 : Analyse de la qualité du code avec pylint et Sonarqube

### Utilisation de pylint

- Exécutez l'outil `pylint` [en ligne de commande](https://pylint.pycqa.org/en/latest/user_guide/run.html) sur le fichier `karma_analysis.py` :

  ```bash
  pylint src/karma_analysis.py
  ```

- Appliquez des corrections pour obtenir au minimum la note de **7**.




### Installation du serveur Sonarqube (nécessite Docker sur votre machine)

- Démarrez un conteneur avec l'image Sonarqube disponible sur DockerHub :

  ```bash
  docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube
  ```

- Accédez au serveur (attendez que Sonarqube ait fini de démarrer dans le conteneur) : <http://localhost:9000/>
- Connectez-vous avec l'utilisateur `admin` et le mot de passe `admin`.
- Démarrez un nouveau projet en mode _"Manually"_.
- Choisissez un nom et enregistrez la clé fournie.
- Choisissez l'analyse locale et générez un token.
- Suivez les instructions jusqu'à obtention de la ligne de commande du client (ie. `sonar-scanner`).

**Note : Si vous voulez réutiliser votre serveur Sonar après avoir arrêté Docker :**

  ```bash
  docker restart sonarqube
  ```

### Installation du client Sonarqube

- Récupérez [l'archive du client correspondant à votre environnement](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/) (liens en bleu sous le numéro de version).
- Décompressez l'archive obtenue dans le répertoire de votre choix.
- Le programme `sonar-scanner` est accessible dans le dossier `bin` de l'archive décompressée.

### Utilisation de Sonarqube

- Placez-vous dans un répertoire contenant les fichiers du projet.
- Passez une commande similaire à celle proposée dans l'interface.
- Visualisez les résultats : dans la partie _"issues"_, vous pouvez retrouver une partie des résultats du `pylint` (_code smells_).

### Utilisation avancée

- Remplacez les éléments de la ligne de commande en utilisant un fichier `sonar-project.properties` : reportez-vous à [la documentation](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/).
- Après avoir utilisé la commande `coverage` pour générer un rapport de couverture en XML, utilisez le paramètre `sonar.python.coverage.reportPaths` dans le fichier `sonar-project.properties` et essayez d'afficher le taux de couverture dans SonarQube.
- Essayez de corriger les tests pour passer en _"Quality Gate A"_.
- À partir de l'onglet _"Project information"_, accédez aux _"Quality Profiles used"_ et rajoutez des règles pour le langage Python.
- Activez le nouveau profil dans `Project Settings > Quality Profiles`.
- Relancez une analyse, évaluez les différences (dette technique, code smells, etc.).

----

## Étape 4 : Respect des normes de codage, Black et pre-commit à la rescousse

Black est un outil de formatage de code qui permet de garantir un style de code cohérent et lisible dans l'ensemble d'un projet. Il est possible de l'utiliser en ligne de commande, en tant que plugin de votre éditeur de code favori ou encore en directement via git.

Pour permettre à Black de s'intégrer à git, nous allons utiliser le package [pre-commit](https://pre-commit.com/). Ce package permet d'ajouter des hooks git qui seront exécutés avant chaque commit. Ces hooks peuvent être des scripts bash, des commandes shell ou encore des commandes python. Dans notre cas, nous allons utiliser le hook `black` qui permet d'appliquer le formatage de code de Black avant chaque commit et garantir qu'aucune ligne de code présente sur le dépôt git ne contrevient aux règles de formatage !

- Ajoutez le fichier `.pre-commit-config.yaml` à la racine du dépôt git :

```yaml
repos:
  # Using this mirror lets us use mypyc-compiled black, which is about 2x faster
  - repo: https://github.com/psf/black-pre-commit-mirror
    rev: 23.9.1
    hooks:
      - id: black
        # It is recommended to specify the latest version of Python
        # supported by your project here, or alternatively use
        # pre-commit's default_language_version, see
        # https://pre-commit.com/#top_level-default_language_version
        language_version: python3.11
```

- Initialisez le hook `black` dans le dépôt git :

  ```bash
  pre-commit install
  ```

- Maintenant, à chaque fois que vous allez faire un commit, le hook `black` va être exécuté et va formater le code pour qu'il respecte les règles de formatage de Black. Si le code n'est pas formaté correctement, le commit sera annulé et vous devrez corriger le code avant de pouvoir le commiter à nouveau.

- Nous vous recommandons d'ajouter Black à votre éditeur de code favori pour formater le code à la volée.
